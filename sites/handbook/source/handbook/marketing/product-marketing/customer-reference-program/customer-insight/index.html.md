---
layout: handbook-page-toc
title: "Customer Insight Page"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

#### Common Interview questions
Interview Questions: (Select the questions we should ask)

**Why GitLab**
- [ ] What insights do you have that might make a good case study today?
- [ ] Describe what your organization does, how the software is used, and how your team helps solve business challenges.
- [ ] What problem were you trying to solve when you were looking at GitLab?
- [ ] What was your process before using GitLab?
- [ ] Why did you choose to replace your current tooling?
- [ ] What were the negative consequences of your previous environment?
- [ ] What would have happened if you hadn't replaced your tooling
- [ ] How was this problem affecting you?
- [ ] What products were you using before using GitLab?
- [ ] What were the required capabilities you were looking for?
- [ ] Why did you choose GitLab?

**What if**
- [ ] What would have happened if you hadn't selected GitLab?

**Feedback on GitLab**
- [ ] How did GitLab solve those problems you were suffering from?
- [ ] Which teams are using GitLab?
- [ ] What do users say about GitLab?
- [ ] How has GitLab's monthly release cycle benefitted your SDLC?
- [ ] Has the regular release cycle supported innovation and collaboration?
- [ ] If your team is not on the most recent version (or within a release or two), why haven't you updated?

**Impact of GitLab**
- [ ] What are the positive business outcomes of introducing GitLab?
- [ ] What are some initial successes resulting from moving to GitLab?
- [ ] Can you share any larger successes?

**Value Driver 1 - Increasing Operational Efficiencies**
- [ ] Has GitLab simplified workflow?
- [ ] How has GitLab enabled cross-functional relationships?
- [ ] How has it helped modernize architecture?
- [ ] What are some initial successes resulting from moving to GitLab?
- [ ] What does your current workflow look like?
- [ ] What other tools do you have plugged into GitLab?
- [ ] Can you describe how your deployments look?
- [ ] Does GitLab integrate/plug into your use of cloud resources?

**Value Driver 2 - Deliver Better Products Faster**
- [ ] Are your processes and tools now more streamlined? Can you describe what the process looks like now?
- [ ] How does having control and visibility of the projects help with planning and governance? 
- [ ] Do your developer teams feel more connected and empowered?
- [ ] How has detecting bugs earlier in the lifecycle impacted the output?

**Value Driver 3 - Reduce Security and Compliance Risk**
- [ ] Are projects delivering on time and on budget?
- [ ] How has the additional security testing impacted your projects?
- [ ] Are audits quicker and seamless now?
 
**Impact metrics of using GitLab**
- [ ] Do you have any metrics available?
- [ ] How has GitLab impacted your cycle time?
- [ ] Have you measured throughput/Lead time? Can you give a before and after?
- [ ] What does your deployment frequency look like?
- [ ] Has there been an impact on change failure rate?
- [ ] Is your MTTR (mean time to resolution) improved?
- [ ] What percentage of your releases are now on time and on budget?
- [ ] Are you catching bugs earlier? Are you catching bugs that would have previously been missed?
- [ ] What percentage of your code is now scanned?
- [ ] What percentage of critical security vulnerabilities that previously escaped development have been caught?
- [ ] Has GitLab helped you achieve the KPIs you set out to solve?

**Customer Use Case Questions**

**Version Control & Collaboration (VC&C)/Source Code Management (SCM)** 
- [ ] How does your team create, manage and protect your project artifacts. (source code, designs, configuration, etc)?
- [ ] How were you managing project assets prior to GitLab?
- [ ] Does having one SCM tool improve your development process (efficiency, speed, quality, etc)?

**Continuous Integration (CI)** 
- [ ] How are you automating software development processes, testing and building code?
- [ ] How does GitLab Continuous Integration add value to your SDLC?
- [ ] How has GitLab Continuous Integration helped improve quality and speed of your delivery? 

**Continuous Delivery (CD)**
- [ ] How are you improving the speed to deploy code?
- [ ] Has using GitLab Continuous Delivery empowered your developers to deploy changes faster?  What's their feedback?
- [ ] Has the automated delivery increased your code output while maintaining quality?

**Development, Security, and Operations (DevSecOps)** 
- [ ] How are you identifying application vulnerabilities during your development process?
- [ ] How has detecting security bugs earlier impacted the SDLC?
- [ ] How has GitLab helped support audits and compliance etc?
- [ ] How has shifting security earlier in the delivery lifecycle impacted your speed to deliver?

**Agile Project Management (Agile)** 
- [ ] How are you managing your team's backlog, sprints, milestones, and future work?
- [ ] How do you have visibility of team results?
- [ ] How do you track/monitor project milestones etc?

**Simplify Development Operations / End to End DevOps (DevOps)** 
- [ ] How has collaboration across teams improved with GitLab?
- [ ] How has productivity or efficiency improved using GitLab? 
- [ ] What other benefits have you realized with GitLab and a single application?

**Cloud Native Approach to Applications Development (CloudNative)**
- [ ] How are you developing and managing cloud native applications? 
- [ ] How has GitLab helped you to manage cloud native/microservices? 
- [ ] What are the benefits of using GitLab's kubernetes integration?

**Infrastructure as Code (GitOps)**
- [ ] How are your operations teams managing different versions of their automation/configuration scripts?
- [ ] How are you testing and validating your infrastructure scripts?
- [ ] How are you integrating with other tools to maintain IAC?

**Deployment Strategy Questions**
- [ ] Can you describe the current deployment strategy you are using with GitLab? (target environment types, on-premise infrastructure and/or public cloud services, some combination of these and/or other resources)?
- [ ] Can you share the background and reasoning behind selecting this current deployment strategy?
- [ ] Were there specific business and/or technical reasons that shaped this strategy?
- [ ] **If customer identified they are using cloud services or plan to** Is your organization consuming or planning to consume cloud services? If so, which ones? and over what timeframe? Can you describe your current cloud deployment architecture-- if applicable?
- [ ] Are you using or planning to use containers? If so, can you describe your use of containers?

**Education Program Customers**
- [ ] How is GitLab advancing research at your University/Research Organization? 	
- [ ] What role does GitLab have in advancing open science?
- [ ] How are you using GitLab in your organization?
- [ ] What do the students and researchers like most about GitLab?
- [ ] What advantages does learning GitLab while in University give to students entering the professional world?
- [ ] How are students and researchers using GitLab to collaborate?

**Open Source Program Customers**
- [ ] How is GitLab impacting the number of contributions in your community?
- [ ] What kind of feedback have you received about GitLab from your community? 
- [ ] Did you need to adapt GitLab to fit your community’s needs? 
- [ ] What are the benefits of moving to GitLab for open source communities?
- [ ] What other services did you use before GitLab? (e.g. for hosting, task management, etc)
- [ ] What other services are you still using? (e.g. for hosting, task management, etc)
- [ ] Did you leverage any other open source programs? (e.g. Packet program, AWS credits, etc)
- [ ] (If not using the Community Edition) Which top tier enterprise features, if any, do you find most helpful? 
- [ ] How are you measuring the value that GitLab is delivering for your open source program? 
