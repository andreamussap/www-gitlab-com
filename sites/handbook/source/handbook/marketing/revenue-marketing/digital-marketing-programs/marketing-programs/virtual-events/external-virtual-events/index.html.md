---
layout: markdown_page
title: "External Virtual Events"
---

# Types of external virtual events

External virtual events are, by definition, not owned and hosted by GitLab. They are hosted by an external third party (i.e. DevOps.com). The goal of external virtual events is to drive net new leads, and we do not promote to our internal database. The various types of external virtual events are below, and involve epic and issue creation, designation of DRIs, and workback schedule definition within the issue due dates.

* [Sponsored Webcasts](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/external-virtual-events/#-sponsored-webcast): A sponsored webcast is hosted on an external partner/vendor platform (e.g: DevOps.com). The partner/vendor is responsible for driving registration, moderating and hosting the webcast on their platform, and delivering a lead list after the event. The goal of a sponsored webcast is net new leads - we do not promote to our existing database.
* [Sponsored Virtual Conferences](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/external-virtual-events/#-sponsored-virtual-conference): In a virtual conference, GitLab will pay a sponsorship fee to receive a virtual booth and sometimes a speaking session slot or panel presence. **Presence of a virtual booth is a requirement for the external virtual event to be considered a Virtual Conference.**  The goal of a sponsored virtual conference is net new leads - we do not promote to our existing database.
* [Executive Roundtable](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/external-virtual-events/#-executive-roundtable):  An executive roundtable is a gathering of high level CxO attendees run as an open discussion between the moderator/host, GitLab expert and delegates. There usually aren't any presentations, but instead a discussion where anyone can chime in to speak. The host would prepare questions to lead discussion topics and go around the room asking delegates questions to answer. The goal of an executive roundtable is net new leads - we do not promote to our existing database.
* [Vendor Arranged Meetings](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/external-virtual-events/#-vendor-arranged-meetings): A vendor arranged meeting is used for campaigns where a third party vendor is organizing one-to-one meetings with prospect or customer accounts. This does not organize meetings set internally by GitLab team members. An example would be a "speed dating" style meeting setup where a vendor organized meetings with prospects of interest to GitLab. The goal of a venor arranged meeting is to generate meetings with accounts of interest that we are finding challenging to break into directly.

## 📌 Sponsored Webcast

*A sponsored webcast is hosted on an external partner/vendor platform (e.g: DevOps.com), with the goal of driving net new leads. The partner/vendor is responsible for driving registration, moderating and hosting the webcast on their platform, and delivering a lead list after the event. The project owner is responsibble for creating the epic and related issue creation, and keeping timelines and DRIs up-to-date. Mareting Ops is responsible for uploading the list to our database and Marketing Programs is responsible for sending post-event follow-up emails (when relevant)*

### Process in GitLab to organize epic & issues

The project owner is responsible for following the steps below to create the epic and related issues in GitLab.

1. Project owner (FMM) creates the main tactic issue
1. Project owner (FMC) creates the epic to house all related issues (code below)
1. Project owner (FMC) creates the relevant issues required (shortcut links in epic code below)
1. Project owner (FMC) associates all the relevant issues to the newly created epic, as well as the original issue
1. Project owner (FMC) sets due dates for each issue, based on agreed upon SLAs between teams for each task, and confirms accurate ownership for each issue

*Note: if the date of the tactic changes, the project owner is responsible for changing the due dates of all related issues to match the new date, and alerting the team members involved.*

### Code for epic

```
<!-- Name this epic: Sponsored Webcast - [Vendor] - [3-letter Month] [Date], [Year] -->

## [Main Issue >>]()

## [Copy document >>]() - [template](https://docs.google.com/document/d/1j43mf7Lsq2AXoNwiygGAr_laiFzmokNCfMHi7KNLjuA/edit)

## :notepad_spiral: Key Details 
* **Project Owner/Field Marketing Manager:** 
* **Field Marketing Coordinator:** 
* **Type:** Sponsored Webcast
* **Official Name:** 
* **Registration URL:** 
* **Sales Segment:** `Large, Mid-Market, or SMB`
* **Sales Region:** `AMER, EMEA, APAC`
* **Sales Territory (if specific):** 
* **Goal:** `Please be specific on the KPI this is meant to impact. For example, drive MQLs against named account list, increase velocity of MQLs > SAOs, increase velocity of early stage opps to close.`
* **Budget:** 
* **Campaign Tag:**  
* **Launch Date:**  [YYYY-MM-DD] 
* [ ] [main salesforce program]()
* [ ] [main marketo campaign]()
* [ ] Campaign UTM - Project Owner/FM to fill in (Format: campaign tag - change to all lowercase, no spaces, hyphens, underscores, or special characters)

## User Experience
[Project Owner/FMM to provide a description of the user journey - from communication plan, to what the user experiences upon receipt, plus triggers on our end like confirmation email and how GitLab fulfils with the vendor, up until receipt by the user and answering whether or not we get confirmation that they received it... what is the anticipated journey after that?]

## Additional description and notes about the tactic
[Project Owner/FMM add whatever additional notes are relevant here]

## Issue creation

* [ ] [Facilitate tracking issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-01-facilitate-tracking) - FMC creates, assigns to FMC or MPM (depending on [program type](https://about.gitlab.com/handbook/marketing/marketing-operations/#marketo-program-and-salesforce-campaign-set-up))
* [ ] [List clean and upload issue created](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=event-clean-upload-list) - FMC creates, assigns to FMM and MOps
* [ ] [Follow up email issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-04-follow-up-email) (*optional*) - FMC creates, assigns to FMM, FMC and MPM
* [ ] [Add to nurture issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-05-add-to-nurture) - FMC creates, assigns to FMM and MPM

**Optional: create the optional issues only if we have rights to recording and content is worth gating*

Add the team label to indicate the team running the event (Example: Field Marketing, Corporate Marketing)   

/label ~"mktg-status::wip" ~"Webcast - Sponsored" 
```

☝️ *Note on campaign utm format: we avoid using special characters due to issues in the past passing UTMs from Bizible to SFDC, the basis for attribution reporting.*

## 📌 Sponsored Virtual Conference

*In a virtual conference, GitLab will pay a sponsorship fee to receive a virtual booth and sometimes a speaking session slot or panel presence, with the goal being net new leads. The owner is responsible for the epic and related issue creation. Mktg-OPs will be responsible for uploading the list to our database and MPMs will be responsible for sending post-event follow-up emails (when relevant).*

**Presence of a virtual booth is a requirement for the virtual event to be considered a Virtual Conference.** [Link to Marketo program template that will be cloned.](https://app-ab13.marketo.com/#ME5121A1)

### Process in GitLab to organize epic & issues

The project owner is responsible for following the steps below to create the epic and related issues in GitLab.

1. Project owner (Corp/FMM) creates the main tactic issue
1. Project owner (Corp/FMC) creates the epic to house all related issues (code below)
1. Project owner (Corp/FMC) creates the relevant issues required (shortcut links in epic code below)
1. Project owner (Corp/FMC) associates all the relevant issues to the newly created epic, as well as the original issue
1. Project owner (Corp/FMC) sets due dates for each issue, based on agreed upon SLAs between teams for each task, and confirms accurate ownership for each issue

*Note: if the date of the tactic changes, the project owner is responsible for changing the due dates of all related issues to match the new date, and alerting the team members involved.*

### Code for epic

```
<!-- Name this epic: Sponsored Virtual Conference - [Vendor] - [3-letter Month] [Date], [Year] -->

## [Main Issue >>]()

## [Copy document >>]() - [template](https://docs.google.com/document/d/1j43mf7Lsq2AXoNwiygGAr_laiFzmokNCfMHi7KNLjuA/edit)

## :notepad_spiral: Key Details 
* **Corp Events/Field Marketing Manager/Requester:** 
* **Field Marketing Coordinator:** 
* **Type:** Sponsored Virtual Conference
* **Official Name:** 
* **Registration URL:** 
* **Sales Segment:** `Large, Mid-Market, or SMB`
* **Sales Region:** `AMER, EMEA, APAC`
* **Sales Territory (if specific):** 
* **Goal:** `Please be specific on the KPI this is meant to impact. For example, drive MQLs against named account list, increase velocity of MQLs > SAOs, increase velocity of early stage opps to close.`
* **Budget:** 
* **Campaign Tag:**  
* **Launch Date:**  [YYYY-MM-DD] 
* [ ] [main salesforce program]()
* [ ] [main marketo campaign]()
* [ ] Campaign UTM - Project Owner to fill in (Format: campaign tag - change to all lowercase, no spaces, hyphens, underscores, or special characters)

## User Experience
[Project owner to provide a description of the user journey - from communication plan, to what the user experiences upon receipt, plus triggers on our end like confirmation email and how GitLab fulfils with the vendor, up until receipt by the user and answering whether or not we get confirmation that they received it... what is the anticipated journey after that?]

## Additional description and notes about the tactic
[Project owner add whatever additional notes are relevant here]

## Issue creation

* [ ] Prep: [Facilitate tracking issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-01-facilitate-tracking) - Corp/FMC creates, assigns to Corp/FMC or MPM (depending on [program type](https://about.gitlab.com/handbook/marketing/marketing-operations/#marketo-program-and-salesforce-campaign-set-up))
* [ ] Post: [List clean and upload issue created](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=event-clean-upload-list) - Corp/FMC creates, assigns to Corp/FMM and MOps
* [ ] Post: [Follow up email issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-04-follow-up-email) (*optional*) - Corp/FMC creates, assigns to Corp/FMM, FMC and MPM
* [ ] Post: [Add to nurture issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-05-add-to-nurture) - Corp/FMC creates, assigns to Corp/FMM and MPM
* [ ] Post*: [Marketo landing page copy issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=marketo-landing-page-copy) - Corp/FMC creates, assign to Corp/FMM and FMC
* [ ] Post*: [Marketo landing page creation issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=marketo-landing-page-creation) - Corp/FMC creates, assign to MPM

<details>
<summary>Corporate Marketing Activation: Expand below for quick links to issues to be created and linked to the epic.</summary>

* [ ] Activate*: [Organic Social Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/issues/new?issuable_template=social-general-request) - Corp creates, assignment in issue
* [ ] Activate*: [Blog Issue](https://gitlab.com/gitlab-com/marketing/growth-marketing/global-content/content-marketing/issues/new#?issuable_template=blog-post-pitch) - Corp creates, assignment in issue
* [ ] Activate*: [PR Announcement Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/issues/new?issuable_template=announcement) - Corp creates, assignment in issue

</details>

**Everything with an * is optional: create the optional issues only if we plan to use those outbound activation channels, and/or have rights to the recording(s) of the content and is worth gating (with a further plan for post-live content activation).*

Add the team label to indicate the team running the event (Example: Field Marketing, Corporate Marketing)    

/label ~"Marketing Programs" ~"mktg-status::wip" ~"Virtual Conference" 
```

## 📌 Executive Roundtable

*An executive roundtable is a gathering of high level CxO attendees run as an open discussion between the moderator/host, GitLab expert and delegates. There usually aren't any presentations, but instead a discussion where anyone can chime in to speak. The host would prepare questions to lead discussion topics and go around the room asking delegates questions to answer. The project owner (field marketing) is responsible for creating the epic and related issue creation, and keeping timelines and DRIs up-to-date. Mareting Ops is responsible for uploading the list to our database and Marketing Programs is responsible for sending post-event follow-up emails (when relevant)* 

### Process in GitLab to organize epic & issues

The project owner is responsible for following the steps below to create the epic and related issues in GitLab.

1. Project owner (FMM) creates the main tactic issue
1. Project owner (FMC) creates the epic to house all related issues (code below)
1. Project owner (FMC) creates the relevant issues required (shortcut links in epic code below)
1. Project owner (FMC) associates all the relevant issues to the newly created epic, as well as the original issue
1. Project owner (FMC) sets due dates for each issue, based on agreed upon SLAs between teams for each task, and confirms accurate ownership for each issue

*Note: if the date of the tactic changes, the project owner is responsible for changing the due dates of all related issues to match the new date, and alerting the team members involved.*

### Code for epic

```
<!-- Name this epic: Executive Roundtable - [Vendor] - [3-letter Month] [Date], [Year] -->

## [Main Issue >>]()

## [Copy document >>]() - [template](https://docs.google.com/document/d/1j43mf7Lsq2AXoNwiygGAr_laiFzmokNCfMHi7KNLjuA/edit)

## :notepad_spiral: Key Details 
* **Project Owner/Field Marketing Manager:** 
* **Field Marketing Coordinator:** 
* **Type:** Executive Roundtable 
* **Official Name:** 
* **Registration URL:** 
* **Sales Segment:** `Large, Mid-Market, or SMB`
* **Sales Region:** `AMER, EMEA, APAC`
* **Sales Territory (if specific):** 
* **Goal:** `Please be specific on the KPI this is meant to impact. For example, drive MQLs against named account list, increase velocity of MQLs > SAOs, increase velocity of early stage opps to close.`
* **Budget:** 
* **Campaign Tag:**  
* **Launch Date:**  [YYYY-MM-DD] 
* [ ] [main salesforce program]()
* [ ] [main marketo campaign]()
* [ ] Campaign UTM - Project Owner/FM to fill in (Format: campaign tag - change to all lowercase, no spaces, hyphens, underscores, or special characters)

## User Experience
[Project Owner/FMM to provide a description of the user journey - from communication plan, to what the user experiences upon reciept, plus triggers on our end like confirmation email and how GitLab fulfils with the vendor, up until receipt by the user and answering whether or not we get confirmation that they received it... what is the anticipated journey after that?]

## Additional description and notes about the tactic
[Project Owner/FMM add whatever additional notes are relevant here]

## Issue creation

* [ ] [Facilitate tracking issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-01-facilitate-tracking) - FMC creates, assigns to FMC or MPM (depending on [program type](https://about.gitlab.com/handbook/marketing/marketing-operations/#marketo-program-and-salesforce-campaign-set-up))
* [ ] [List clean and upload issue created](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=event-clean-upload-list) - FMC creates, assigns to FMM and MOps
* [ ] [Follow up email issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-04-follow-up-email) (*optional*) - FMC creates, assigns to FMM, FMC and MPM
* [ ] [Add to nurture issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-05-add-to-nurture) - FMC creates, assigns to FMM and MPM

**Optional: create the optional issues only if we have rights to recording and content is worth gating*

/label ~"mktg-status::wip" ~"Field Marketing" ~"Executive Roundtable"
```

## 📌 Vendor Arranged Meetings  

*A vendor arranged meeting is used for campaigns where a third party vendor is organizing one-to-one meetings with prospect or customer accounts, ideally connecting our team with target accounts that are challenging to set meetings with directly. This does not include meetings set internally by GitLab team members. An example would be a "speed dating" style meeting setup where a vendor organized meetings with prospects of interest to GitLab. The project owner (field marketing commonly) is responsible for creating the epic and related issue creation, and keeping timelines and DRIs up-to-date. Mareting Ops is responsible for uploading the list to our database and Marketing Programs is responsible for sending post-event follow-up emails (when relevant)* 

### Process in GitLab to organize epic & issues

The project owner is responsible for following the steps below to create the epic and related issues in GitLab.

1. Project owner (FMM) creates the main tactic issue
1. Project owner (FMC) creates the epic to house all related issues (code below)
1. Project owner (FMC) creates the relevant issues required (shortcut links in epic code below)
1. Project owner (FMC) associates all the relevant issues to the newly created epic, as well as the original issue
1. Project owner (FMC) sets due dates for each issue, based on agreed upon SLAs between teams for each task, and confirms accurate ownership for each issue

*Note: if the date of the tactic changes, the project owner is responsible for changing the due dates of all related issues to match the new date, and alerting the team members involved.*

### Code for epic

```
<!-- Name this epic: Vendor Arranged Meeting - [Vendor] - [3-letter Month] [Date], [Year] -->

## [Main Issue >>]()

## [Copy document >>]() - [template](https://docs.google.com/document/d/1j43mf7Lsq2AXoNwiygGAr_laiFzmokNCfMHi7KNLjuA/edit)

## :notepad_spiral: Key Details 
* **Project Owner/Field Marketing Manager:** 
* **Field Marketing Coordinator:** 
* **Type:** Vendor Arranged Meetings 
* **Official Name:** 
* **Registration URL:** 
* **Sales Segment:** `Large, Mid-Market, or SMB`
* **Sales Region:** `AMER, EMEA, APAC`
* **Sales Territory (if specific):** 
* **Goal:** `Please be specific on the KPI this is meant to impact. For example, drive MQLs against named account list, increase velocity of MQLs > SAOs, increase velocity of early stage opps to close.`
* **Budget:** 
* **Campaign Tag:**  
* **Launch Date:**  [YYYY-MM-DD] 
* [ ] [main salesforce program]()
* [ ] [main marketo campaign]()
* [ ] Campaign UTM - Project Owner/FM to fill in (Format: campaign tag - change to all lowercase, no spaces, hyphens, underscores, or special characters)

## User Experience
[Project Owner/FMM to provide a description of the user journey - from communication plan, to what the user experiences upon reciept, plus triggers on our end like confirmation email and how GitLab fulfils with the vendor, up until receipt by the user and answering whether or not we get confirmation that they received it... what is the anticipated journey after that?]

## Additional description and notes about the tactic
[Project Owner/FMM add whatever additional notes are relevant here]

## Issue creation

* [ ] [Facilitate tracking issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-01-facilitate-tracking) - FMC creates, assigns to FMC or MPM (depending on [program type](https://about.gitlab.com/handbook/marketing/marketing-operations/#marketo-program-and-salesforce-campaign-set-up))
* [ ] [List clean and upload issue created](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=event-clean-upload-list) - FMC creates, assigns to FMM and MOps
* [ ] [Follow up email issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-04-follow-up-email) (*optional*) - FMC creates, assigns to FMM, FMC and MPM
* [ ] [Add to nurture issue created](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=MPM-05-add-to-nurture) - FMC creates, assigns to FMM and MPM

**Optional: create the optional issues only if we have rights to recording and content is worth gating*

/label ~"mktg-status::wip" ~"Field Marketing" ~"Vendor Arranged Meetings"
```

## Adding external virtual events into the calendar

The [external virtual events](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV8xOGVqOHN0NmlxajZpYXB1NTNrajUzNHBsa0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t) calendar will be used to log all planned and scheduled sponsored webcasts and virtual conferences. **The purpose of this calendar is to provide visibility and help the sponsoring team minimize topic overlap with other GitLab virtual events happening around the same time and to provide executive visibility into all GitLab external virtual events.**

**DRI adding to external virtual events calendar: Sponsor owner**

##### Planned external virtual events

As soon as you create the epic for the sponsored virtual event, add the event to the external virtual events calendar by creating an event on the day the sponsored virtual event will be live.Make sure to also include the link to the epic in the calendar description.
* For sponsored webcast please use the following naming convention `[Hold WC sponsored] Event title` (e.g: `[Hold WC sponsored] Securing your pipeline with GitLab and WhiteSource`).
* For sponsored virtual conferences, please use the following naming convention  `[Hold VC sponsored] Event title` (e.g: `[Hold VC sponsored] Predict 2021`).
* For executive round tables please use the following naming convention `[Hold ER sponsored] Event title` (e.g: `[Hold ER sponsored] DevOps 101`).
* For vendor arranged meetings, please use the following naming convention  `[Hold VA sponsored] Vendor Name Region` (e.g: `[Hold VA sponsored] Captive Eight APAC `).

##### Confirmed external virtual events 

Once the sponsorship has been confirmed, go to your calendar event and remove `Hold` from the event title. **Note:** In the spirit of efficiency and to avoid creating multiple calendar invites, please include the epic or issue, add the marketing DRI, any GitLab speakers and/or attendees (SALs, SAs, etc.), as well as any other team members who would benefit from being included in the calendar invite as this invite will provide a hold for team members participating in the event. The 3rd party sponsor will send out additional event details separately.
* For sponsored webcasts, change the event title to `[WC sponsored] Event title` (e.g: `[WC sponsored] Securing your pipeline with GitLab and WhiteSource`).
* For sponsored virtual conferences, change the event title to  `[VC sponsored] Event title` (e.g: `[VC sponsored] Predict 2021`).
* For executive round tables, change the event title to `[ER sponsored] Event title` (e.g: `[ER sponsored] DevOps 101`).
* For sponsored virtual conferences, change the event title to  `[ER sponsored] Vendor Name Region` (e.g: `[ER sponsored] Captive Eight APAC`).

## Rescheduling external virtual events

Once the DRI has identified that the date will change, **DRI** will:  

* Update the event date and the campaign tag on the main event issue and in the Budget Document.  
* Tag the relevant internal contacts for the event in a comment to notify them that the date has changed.
* In the main event issue, ping the GL Accountant (@GGGONZALEZ) with the old campaign tag to be removed from Netsuite and the new campaign tag to be added in Netsuite.  
* If event is on the Events Page, [submit MR](https://about.gitlab.com/handbook/marketing/events/#how-to-add-events-to-aboutgitlabcomevents) to update the date.
* If event is on a specific Virtual Events Calendar, move to the new date and adjust calendar information.

If the date changes after the sub-issues, epic, and tracking for the event have been created, the **DRI/FMC** must make the following updates:

* SFDC: Change the ISO date in the SFDC Campaign name to the new date. Update the campaign start date to 30 days prior to the new date, and the end date to 60 days after the new date. This change will automatically sync with Marketo.
* Marketo: Change the ISO date in the Marketo Program to the new date and update the tokens.
* GitLab: Update event date in epic and sub-issues. Update due dates and email deployment dates.  

## Cancelling external virtual events

Once the DRI has identified that an event is cancelled, **DRI** will:

* Update the event issue with [Cancelled] in the event title.
* Tag the relevant internal contacts for the event in a comment to notify them that the event has been cancelled.
* Close the main Field/Corporate Marketing event issue and epic.
* Cancel the event line item from the regional tab.
   * If receiving a refund: `Field Marketing DRI` will add requested refund info directly into the [FM Cancellation Tab](https://docs.google.com/spreadsheets/d/1QC6P0VRWwJheOlGB-9bX8JIF8_4UY3h1cGVT_gacv5M/edit#gid=1753355316&range=A2) of the Budget Doc. `Corporate Marketing DRI` will track cancellation refunds in [this budget doc](https://docs.google.com/spreadsheets/d/1WVWZjSF6f5jAFqHO4hXcV8mN975ITT4eXScSn0F_FU8/edit#gid=1109485360). 
   * See [instructions](https://gitlab.com/gitlab-com/Finance-Division/finance/-/issues/2287) on how to technically obtain a refund with finance. 
* If event is on the Events Page, [submit MR](https://about.gitlab.com/handbook/marketing/events/#how-to-add-events-to-aboutgitlabcomevents) to remove.
* If event is on a specific Virtual Events Calendar, cancel off of the calendar.

If an online event is cancelled after the sub-issues, epic, and tracking for the event have been created, the **DRI/FMC** will:

* SFDC: Change the event campaign status to `Aborted`. No other changes.
* Marketo: No changes

## Post event

###  Posting external virtual event recordings to youtube

Follow this handbook documentation on how to [upload external webcast recordings](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/#uploading-conversations-to-youtube) to the [GitLab branded Youtube channel](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/#channels).

### Gating external webcasts

`Please Note: via [FY21-22 Segment Marketing Plan](https://docs.google.com/presentation/d/1p4EmaoSb35d8ZnjKags1gUGF5T9afJW6RhBO8TR_VgA/edit#slide=id.g8d20cd3f15_1_15), a first principle for Demand Generation is to "offer compelling ungated content journeys that drive to proven web CTAs" - as such, the plan for further gating is on pause until the plan is developed for what will be gated, and use of Pathfactory for ungated persona-based content journey (target date for plan: 2020-09-14)`

<!--
Note from Jackie: Leaving the code here, but it will not be visible. To be removed/updated upon plan for ungating content (est. delivery 2020-09-14)

To maximize the return on time investment for creating gated webcast landing pages, we will only gate post event recording for an external webcast if it meets the criterias outlined below.

#### Non-Partner webcast

The recording meets all of the following criterias:
1. Content solidifies GitLab use case or existing campaign messaging.
2. Future gated page has an omni-channel (min 2) promotion plan.

#### Partner webcast

The recording meets all of the following criterias:
1. Select or High priority partner: Listed as high priority on the [Alliances Technology Dashboard](https://docs.google.com/spreadsheets/d/1-EE7vChGkDeyJxoM-LjVmUdwYwboxBmq8_42hjHGw_w/edit#gid=0) or is a Select channel partner.
2. Content solidifies GitLab use case or existing campaign messaging.
3. Future gated page has an omni-channel (min 2)  promotion plan.

OR  

Ungated video garners 550 youtube views within the first 7 days of posting.

*Note: The 550 min threshold is based on the avg of top 10 videos on gitlab branded youtube channel between 8/11/20 - 8/18/20.*

-->
