---
layout: handbook-page-toc
title: "Observability Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Who We Are

The Observability team owns the monitoring and alerting infrastructure for GitLab.com, as well as our caching/queuing infrastructure.

Observability is:

| Person | Role |
| ------ | ------ |
|[Anthony Sandoval](/company/team/#ansdval)|[Engineering Manager, Reliability](https://about.gitlab.com/job-families/engineering/engineering-management-infrastructure/#engineering-manager-reliability)|
|[Cindy Pallares](/company/team/#cindy)|[Site Reliability Engineer](/job-families/engineering/site-reliability-engineer/)|
|[Ben Kochie](/company/team/#bjk-gitlab)|[Senior Site Reliability Engineer](/job-families/engineering/site-reliability-engineer/)|
|[Igor Wiedler](/company/team/#igorwwwwwwwwwwwwwwwwwwww)|[Site Reliability Engineer](/job-families/engineering/site-reliability-engineer/)|
|[Michal Wasilewski](/company/team/#mwasilewski-gitlab)|[Site Reliability Engineer](/job-families/engineering/site-reliability-engineer/)|
|[Matt Smiley](/company/team/#msmiley)|[Senior Site Reliability Engineer](/job-families/engineering/site-reliability-engineer/)|
|[Craig Furman](/company/team/#craigf)|[Senior Site Reliability Engineer](/job-families/engineering/site-reliability-engineer/)|


## Vision

*WIP*

## Tenets

*WIP*