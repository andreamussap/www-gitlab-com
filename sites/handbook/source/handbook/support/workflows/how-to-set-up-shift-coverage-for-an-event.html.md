---
layout: handbook-page-toc
title: Scheduling Support Coverage for a Company-wide Event
category: References
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

##### Overview

Use this workflow when you want to get a coverage schedule, including PD, going for an event like Contribute

---
##### Workflow

##### Building a Shift Schedule

Take a look at this [example](https://docs.google.com/spreadsheets/d/1HcGQNfjmNRapB3gpxTcIzn2zf_ppg8SOVuhd5t1cH0I/edit#gid=278887560)

1. Determine the destination timezone and how it maps to the regions of support
1. Determine who will be attending the event and who will remain in their region.
1. Based on the above, build a spreadsheet with an appropriate number of shifts to get coverage for regions
   1. Shift length could be variable, but 2 hours seems to have worked well.
   1. Number of people per shift will have to be determined by the coverage gaps

##### Building a PD Schedule
- People on shift will handle emergencies
- It may be preferable to create a PD schedule to help facilitate emergency coverage.

Build a PD schedule:
1. Create a new rotation with shift lengths equal to the coverage sheet
1. Add managers in and round-robin through them
   1. Pay attention to the hours of the rotation - it may not be necessary to have 24 hour coverage if some people are in place

Build an Escalation Policy:
1. Escalate first to the PD schedule you just created
1. After some minutes escalate to the team members present at the event
1. Finally, escalate to the 'normal' PD rotation

Updated the Customer Emergencies Service:
1. Update the "Customer Emergencies" service to use the temporary escalation policy
1. Test it out by initiating an emergency
