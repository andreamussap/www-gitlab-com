---
layout: handbook-page-toc
title: Solutions Architects Handbook
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

[**SA Practices**](/handbook/customer-success/solutions-architects/sa-practices) - [**Sales Plays**](/handbook/customer-success/solutions-architects/sales-plays) - [**Tools and Resources**](/handbook/customer-success/solutions-architects/tools-and-resources) - [**Career Development**](/handbook/customer-success/solutions-architects/career-development) - [**Demonstration**](/handbook/customer-success/solutions-architects/demonstrations) - [**Processes**](/handbook/customer-success/solutions-architects/processes)

Solutions Architects (SA) are the trusted advisors to GitLab prospects and customers during the presales motion, demonstrating how the GitLab application and GitLab Professional Services address common and unique business requirements. SA's add value to GitLab by providing subject matter expertise and industry experience throughout the sales process.

SA's identify and propose comprehensive solutions to complex business problems and workflow scenarios after compiling inputs from customer conversations. Desired inputs include pain points, role-based visibility concerns, opportunities for improved efficiencies, current technologies and tools, corporate initiatives, target outcomes and more. SA's also act as the technical liaison between the sales team and other groups within GitLab, engaging GitLab employees from other teams as needed in order meet the needs of the customer.

## Role & Responsibilities

See the [Solutions Architect role description](/job-families/sales/solutions-architect/)

In addition to core responsibilities, Solutions Architects may assist in other client-facing activities aligned to Content Marketing or Strategic Marketing such as blogs, videos, webinars, presentations and industry trade show presence. Some examples are listed below:

- GitLab Security Overview [Video](https://www.youtube.com/watch?v=SP0VSH-NqJs)
- GitLab High Availability and Geo Overview [Video](https://www.youtube.com/watch?v=fji7nvmOHNQ)
- GitLab GitHub Migration [Video](https://www.youtube.com/watch?v=VYOXuOg9tQI)
- GitLab Source Code Management [Video](https://www.youtube.com/watch?v=P6jD966jzlk)
- GitLab Blog [Contribution](/blog/2018/02/20/whats-wrong-with-devops/)
- GitLab with Rancher and EKS [Video](https://www.youtube.com/watch?v=kUwHBIFXciY)

## Changing the Solutions Architects Handbook Content

This is the Solution Architect handbook. If you see any typos, copywriting improvements or content clarifications you'd like to contribute, please create a merge request and ask your managers to merge. Please add the label "SA-HANDBOOK" to your merge request, and post a link to the [solutions-architects](https://gitlab.slack.com/archives/C01788YAY58) slack channel, mentioning @sa_leaders, as a heads up and so we can thank you! Since we have a [bias for action](/handbook/values/#bias-for-action), we trust your judgement.
