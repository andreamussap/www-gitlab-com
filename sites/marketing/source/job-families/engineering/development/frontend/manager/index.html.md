---
layout: job_family_page
title: "Frontend Manager, Engineering"
---

The Frontend Manager, Engineering specializes in Frontend Engineering Management as a manager of people. Managers in Engineering at GitLab see their team as their product. While they are technically credible and know the details of what engineers work on, their time is spent safeguarding their team's health, hiring a world-class team, and putting them in the best position to succeed. They own the delivery of product commitments and are always looking to improve productivity. They must also coordinate across departments to accomplish collaborative goals.

## Responsibilities

* Seek to build out a great team
* Make your team happy and successful
* Improve processes to make your team more effective
* Hold regular 1:1's with all members of your team
* Plan and execute long term strategies that benefit the team and the product stage
* Conduct code reviews, and make technical contributions to product architecture
* Get involved in solving bugs and delivering small features
* Foster technical decision making on the team, and make final decisions when necessary
* Discern engineering metrics and seek to improve them


## Requirements

* Collaborate effectively with others
* Ability to acheive consensus amongst stakeholders
* 5 years or more experience
* 2 years or more experience in a leadership role with current technical experience
* You have an expert comprehension of core web and browser concepts (eg. how does JavaScript handle asynchronous code)
* You have in-depth experience with CSS, HTML, and JavaScript
* You have excellent written and verbal communication skills
* Our [values](/handbook/values) of collaboration, results, efficiency, diversity, iteration, and transparency resonate with you
* Ability to use GitLab

## Nice-to-haves

* You have experience contributing to open source software
* You have experience working with modern frontend frameworks (eg. React, Vue.js)
* You have working knowledge of Ruby on Rails
* You have domain knowledge relevant to the product stage in which you are looking to join (eg. someone with CI/CD experience applying for the Verify & Release team)

## Levels

* [Intermediate](https://about.gitlab.com/job-families/engineering/development/frontend/intermediate)
* [Senior](https://about.gitlab.com/job-families/engineering/development/frontend/senior)
* [Staff](https://about.gitlab.com/job-families/engineering/development/frontend/staff)
* [Manager](https://about.gitlab.com/job-families/engineering/development/frontend/manager)

## Job Grade

The Frontend Engineering Manager is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

## Performance Indicators

Frontend Engineering Managers have the following job-family performance indicators.

* [Mean Time to Merge (MTTM)](/handbook/engineering/development/performance-indicators/#mean-time-to-merge-mttm)
* [Response to Community SLO](/handbook/engineering/development/performance-indicators/#response-to-community-slo)
* [Hiring Actual vs. Plan](/handbook/engineering/performance-indicators/#engineering-hiring-actual-vs-plan)
* [Team/Group Throughput](/handbook/engineering/development/performance-indicators/#throughput)
* [Team/Group MR Rate](/handbook/engineering/development/performance-indicators/#mr-rate)
* [Diversity](/handbook/engineering/performance-indicators/#diversity)
* [Handbook Update Frequency](/handbook/engineering/performance-indicators/#handbook-update-frequency)
* [Team Member Retention](/handbook/engineering/performance-indicators/#team-member-retention)

## Specialties
### {Name of Specialty - i.e. Security, Monitor, Create, etc.}
TBD

#### {Specialty} Requirements
* TBD

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team).

* Selected candidates will be invited to a 30 minute [screening call](/handbook/hiring/#screening-call) with our Technical Recruiters
* Next, candidates will be invited to a 60 minute interview with the Hiring Manager.
* Candidates will then be invited to a 45 minute peer interview with another Frontend Engineering Manager
* Candidates will then be invited to a 45 minute direct report interview
* Candidates will then be invited to a 60 minute interview with our Senior Director of Development
* Successful candidates will subsequently be made an offer

Additional details about our process can be found on our [hiring page](/handbook/hiring).
