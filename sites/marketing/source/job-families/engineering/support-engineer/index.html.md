---
layout: job_family_page
title: "Support Engineer"
---

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/fLTs1oiKabI" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

This position is 100% remote.

## Support Engineer

### Job Grade 

The Support Engineer is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### What you can expect in a Support Engineer role at GitLab:

As a Support Engineer, you will be:

* resolving customer issues via email and video conferencing
* collaborating with our [Product](https://about.gitlab.com/handbook/product/) and [Development](https://about.gitlab.com/handbook/engineering/development/) Teams to build new features and fix bugs.
* creating or updating documentation based on customer interactions.
* working hard to solve customer problems while delighting them along the way
* available for occasional weekend on-call coverage (day-time only).
* working alongside Product Managers to define and shape the product goals, roadmap, priorities, and strategy based on your frontline knowledge of customer needs.
* continually researching and learning the current and future best practices of using GitLab
* participating in our hiring processes by reviewing applications and assessments, and by participating in interviews

### Projects you might work on:

When you’re not tackling difficult customer challenges, you’ll have a lot of freedom to work on things that will make your life, and the lives of your coworkers, easier. Current and past support engineers have:

* created a tool to [quickly analyze strace output](https://gitlab.com/gitlab-com/support/toolbox/strace-parser)
* built and maintained tooling to handle our call scheduling
* scripted a [solution to capture the state of a customer’s server](https://gitlab.com/gitlab-com/support/toolbox/gitlabsos) for easier troubleshooting
* added functionality to [ChatOps](https://docs.gitlab.com/ee/ci/chatops/) to make it easier to identify user accounts on GitLab.com
* written a [Chrome Extension to route downloads from Zendesk tickets into organized folders](https://gitlab.com/gitlab-com/support/toolbox/zd-dl-router)
* … done even more!

## Requirements
### You should apply if:

### You've got the personality
* you're a natural communicator and delight in using those skills to help others.
* you love exploring new technologies and figuring things out the hard way.
* you enjoy solving many small problems per day.

### You're a support professional
* you’ve got 2+ years of experience in a support or other service-oriented customer facing role.
* within the last 5 years, you've worked at one company for at least 2 years.
* you’re experienced in writing support content.
* you’re experienced in managing cases throughout the entire support lifecycle from initial customer inquiry to triage and reproduction, writing bug reports for hand off to the development team, and case resolution.
* you have experience being on-call and can jump into a complex situation and make sense of and communicate clearly to stakeholders what's going on.
* you're comfortable using support platforms such as ZenDesk and Salesforce.

### You've got the technical acumen
* you have knowledge of DevOps methodologies and you appreciate the value technologies like Serverless and Kubernetes bring to the software deployment and development process.
* you’re able to communicate complex technical topics to customers and coworkers of varying technical skill levels.
* you have excellent Ruby on Rails knowledge and are fluent on the Rails console, or you're proficient in working with another MVC framework (Django, Laravel or others) and will be able to pick up Rails quickly.
* you’re experienced with Git and CI/CD.
* you have the additional experience that our [areas of focus](#support-engineering-areas-of-focus) might require.

### You'll be able to thrive at GitLab
* you can successfully complete a [background check](/handbook/people-group/code-of-conduct/#background-checks).
* you can demonstrate excellent spoken and written English.
* you have the ability to use GitLab.
* our [values](https://about.gitlab.com/handbook/values/) of collaboration, results, efficiency, diversity, iteration, and transparency resonate with you.


### Support Engineering Areas of Focus
Support Engineers may focus in one of the following areas, so there may be some different requirements depending on your assignment.

#### Solutions Support Focus
Support Engineers who focus on Solutions Support primarily work with Customers who use GitLab self-managed.
They focus on the hard problems of GitLab at scale: performance,
architecture, and finding those weird edge cases that need to get surfaced as
well-researched bug reports or notes in our documentation.

In addition to the above responsibilities, Support Engineers with this focus will:
- participate in the on-call rotation to give 24/7 emergency customer response for our self-managed customers.

Additional Requirements:
- excellent Linux systems administration knowledge (LFCE or RHCE equivalent knowledge).

#### Application Support Focus
Support Engineers who focus on Application Support primarily work with GitLab end-users and the company-level
administrators who support them. They are experts at the individual features that make up the GitLab application,
and make sure that their expertise translates into customer best practices. They support a mix of self-managed
and GitLab.com customers.

In addition to the above responsibilities, Support Engineers with this focus will:
- participate in the GitLab.com Incident Management rotation to give 24/7 emergency customer response in coordination with the Production team.

Additional Requirements:
- you are process-oriented and comfortable suggesting and implementing improvements to the support workflow.

### [What it’s like to work here](https://about.gitlab.com/jobs/faq/#whats-it-like-to-work-at-gitlab) at GitLab:

The [culture](https://about.gitlab.com/company/culture/) here at GitLab is something we’re incredibly proud of. Because GitLabbers are currently located in over 60 different countries, you’ll spend your time collaborating with kind, talented, and motivated colleagues from across the globe. Some of the [benefits](/handbook/total-rewards/benefits/) you’ll be entitled to vary by the region or country you’re in. However, all GitLabbers are fully remote and receive a "no ask, must tell" paid-time-off policy, where we don’t count the number of days you take off annually. You can work incredibly flexible hours, enabled by our [asynchronous approach](https://about.gitlab.com/handbook/communication/) to communication. We’ll also help you set up your [home office environment](https://about.gitlab.com/handbook/spending-company-money/), pay for your membership to a co-working space, and contribute to the [travel costs](https://about.gitlab.com/handbook/incentives/#visiting-grant) associated with meeting other GitLab employees across the world.

Also, every year or so, we’ll invite you to our [Contribute event](https://about.gitlab.com/company/culture/contribute/).

Our hiring process for this Support Engineer position typically follows five stages. The details of this process and the compensation for this role can be found in our [hiring process](#hiring-process).


### How you'll grow in the role:

### Senior Support Engineer

A Senior Support Engineer is a more experienced engineer who continues to contribute as an individual while operating with equal comfort at the level of team enablement. That is, they work at the team level to increase *every member's* ability to contribute. With the wider focus, the specializations of the Intermediate role begin to disappear.

#### Job Grade 

The Senior Support Engineer is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

Generally they meet the following criteria. They:

- are involved in mentoring teammates on new technologies and new GitLab features.
- possess expert debugging skills.
- submit merge requests to resolve GitLab bugs.
- drive feature requests based on customer interactions.
- contribute to one or more complementary projects.

A Senior Support Engineer may be interested in exploring Support Management as an alternative at this point. See [Engineering Career Development](/handbook/engineering/career-development) for more detail.

### Staff Support Engineer

#### Job Grade 

The Staff Support Engineer is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

A Senior Support Engineer will be promoted to Staff Support Engineer when they have
demonstrated significant leadership and impact; typically around resolving customer issues. This may
involve any type of consistent performance above and beyond the senior level. For example, a Staff Support Engineer might:

-  regularly submit merge requests for customer reported/requested GitLab bugs and feature proposals.
-  work across functional groups to deliver on projects relating to customer experience and success.
-  write in-depth documentation and clarify community communications that share knowledge and radiate GitLab's technical strengths.
-  possess the ability to create innovative solutions that push GitLab's technical abilities ahead of the curve.
-  identify significant projects that result in substantial cost savings or revenue increases.
-  define and solve important architectural issues based on their extensive customer knowledge.

## Career Ladder

For more details on the engineering career ladders, please review the [engineering career development](/handbook/engineering/career-development/#roles) handbook page.

## How you'll be measured
### Performance Indicators

Support Engineers have the following job-family Performance Indicators.

* [Customer satisfaction with Support](/handbook/support/performance-indicators/#support-satisfaction-ssat)
* [Maintain at least average monthly tickets](/handbook/support/performance-indicators/#average-daily-tickets-closed-per-support-team-member)
* [Service Level Agreement](/handbook/support/performance-indicators/#service-level-agreement-sla)
* [Ticket deflection through documentation updates](/handbook/support/#ticket-deflection-through-documentation)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team).

* Selected candidates may receive a short questionnaire from our Global Sourcers.
* Next, candidates will receive a technical assessment via email by the Recruiter. Candidates have 6 week days to complete the assessment, upon request they can have move time to complete it. The technical assessment reviewer will have 2 week days to review the assessment.
* Qualified candidates will be invited to schedule a 30-minute screening call with our Global Recruiters.
* NOTE: For candidates directly sourced, the 30-minute screening call may precede the technical assessment. Then, if the screening is successful the candidate must still receive, complete and pass the technical assessment before being scheduled for a technical interview.

* Next, candidates will move to the first round of interviews
  * 90-Minute Technical Interview with a member of the Support team.
    - The Technical Interview will involve live break-fix/bug-fix scenarios as well as customer scenarios.  You will need to have access to a terminal with Bash or similar. You will also need to have an SSH key pair installed locally so you can connect to the server. Windows users must have ‘Git Bash for Windows’ installed prior to the call. If the Technical Interview is not passed, the Behavioral Interview will be canceled.
  * 60-Minute Behavioral Interview with a Support Engineering Manager
* Next, candidates will move to the second round of interviews
  * 60-Minute Interview with a Senior Manager of Support
* Successful candidates will subsequently be made an offer.

Additional details about our process can be found on our [hiring page](/handbook/hiring).
