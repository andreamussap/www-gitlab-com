---
layout: job_family_page
title: "Product Manager"
---

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/DVLOyaRbAoM" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Role

Product Managers at GitLab have a unique opportunity to define the future of the
entire [DevOps lifecycle](https://about.gitlab.com/stages-devops-lifecycle/). We
are working on a [single application](/handbook/product/single-application) that
allows developers to invent, create, and deploy modern applications.

We want to facilitate [Concurrent DevOps](https://about.gitlab.com/concurrent-devops/) so that teams can work simultaneously instead of sequenctially, unleasing collaboration across organizations.

We work in a very unique way at GitLab, where flexibility and independence meet
a high paced, pragmatic way of working. And everything we do is [in the open](https://about.gitlab.com/handbook).

We are looking for talented product managers that are excited by the idea to
contribute to our vision. We know there are a million things we can and want to
implement in GitLab. Be the one making decisions.

We recommend looking at our [about page](/company/) and at the [product handbook](https://about.gitlab.com/handbook/product/)
to get started.

### Responsibilities

- [Validation Track](/handbook/product-development-flow/#validation-track) Skills
  - Adept at qualitative customer interviewing
  - Familiar with prioritization frameworks like RICE to organize opportunity backlogs
  - Capable of deriving key insights and patterns from customer interviews, and using that input to clarify problem statements
  - Proficient at story mapping, to break epics down into smaller MVC issues
  - Proficient at collaborating with Design on protoypes to bring potential solutions to life
  - Follow the Validation Track to understand customer problems and validate new opportunies
- [Build Track](/handbook/product-development-flow/#build-track) Skills
  - Adept at breaking epics and issues down into MVC's
  - Knowledgeable about GitLab's product and the relevant product domain(s)
  - Knowledgeable about GitLab's architecture, API's, and tech stack
  - Capable of running a demo anytime
  - Able to make highly informed prioritization & tradeoff decisions with engineering
  - Able to discuss & evaluate technical architecture recommendations from eng
  - Responsible for the health of working relationships with peers in the Group
  - Familiar and comfortable with agile development methodologies
- Business Skills
  - Understands and communicates the business value of epics and issues
  - Sets success metrics for epics and issues, and tracks metrics post-launch to guide investment in iterative improvements
  - Spends up to 20% of time researching & defining category vision & strategy
- Communication Skills
  - [Bring ideas to reality](/handbook/product/product-principles/#bringing-ideas-to-reality) by surfacing ideas early and collection feedback
  - Capable written and verbal communicator internally and externally. Drives clarity in area
  - Trusted resource for customer calls and meetings
  - Builds rapport with stakeholders to align around priorities
  - Self aware and understands how their interactions impact others
  - Takes action to improve behavior based on impact to others
- Two to four years of relevant experience or equivalent combination of experience and education

### You are _not_ responsible for

- **A team of engineers:** you will take the lead in decisions about the
  product, but not manage the people implementing it
- **Capacity planning:** you will define priorities, but the Engineering Manager
  evaluates the amount of work possible
- **Shipping in time:** you will work in a group, but the group is responsible
  for shipping in time, not you

<a id="base-pm-requirements"></a>

### Base Requirements Across All Levels

- Experience in product management
- Strong understanding of Git and Git workflows
- Knowledge of the developer tool space
- Strong technically: you understand how software is built, packaged, and deployed
- Passion for design and usability
- Highly independent and pragmatic
- Excellent proficiency in English
- You are living wherever you want and are excited about the [all remote](https://about.gitlab.com/company/culture/all-remote/) lifestyle
- You share our [values](/handbook/values), and work in accordance with those values
- Ability to use GitLab
- Bonus points: experience with GitLab
- Bonus points: experience in working with open source projects

### Primary Performance Indicator for the Role
  - [Stage Monthly Active Users](https://about.gitlab.com/handbook/product/metrics/#adoption)

***

## Career paths

### In role levels

Read more about [levels](/handbook/hiring/#definitions) at GitLab here. Within the Product Management Career Track we use the [Product Management Career Development Framework](https://about.gitlab.com/handbook/product/product-manager-role/#product-management-career-development-framework) to determine requirements for PM roles.

#### Intermediate Product Manager

Beyond the [base requirements](#base-pm-requirements), Product Manager requirements are outlined in the [Product Management Career Development Framework](https://about.gitlab.com/handbook/product/product-manager-role/#product-management-career-development-framework).

##### Job Grade

The Product Manager is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Product Manager

The Senior Product Manager role extends the [Product Manager](#base-pm-requirements) role. Senior Product Managers are expected to be experts in their product domain and viewed as such to the community and internally at GitLab. They are expected to prioritize and manage their products with minimal guidance from leadership or other product team members.

##### Job Grade

The Senior Product Manager is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

##### Responsibilities

**Drive the product in the right direction**
* Consistently deliver outsized impact to [IACV](https://about.gitlab.com/handbook/finance/operating-metrics/#incremental-annual-contract-value-iacv) or other [Product and CEO KPIs](https://about.gitlab.com/handbook/ceo/kpis/)

**Take an active role in defining the future**
* Mastery of the competitive and market landscape of their product domain and understanding how this landscape impacts the product roadmap

**Manage the product lifecycle end-to-end**
* Document ROI or impact for a given action, feature, or prioritization decision
* Execute to deliver outsized results on the aforementioned ROI/impact analysis

**Engage with stakeholders in two-way communication**
* Represent GitLab as a product and domain expert in front of industry analysts, strategic customers, industry events/conferences, and other events
* Ability to present to C-level executives both internally at GitLab and externally to customers and prospects

**Lead by example**
* Mentor less experienced Product Managers to enable them add more value sooner

##### Requirements

Beyond the [base requirements](#base-pm-requirements), Senior Product Manager requirements are outlined in the [Product Management Career Development Framework](https://about.gitlab.com/handbook/product/product-manager-role/#product-management-career-development-framework).

#### Principal Product Manager

The Principal Product Manager role extends the Senior Product Manager plus [Product Manager](#base-pm-requirements) role expectations.

##### Job Grade

The Principal Product Manager is a [grade 9.5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

##### Responsibilities

**Bring special expertise in a specific domain or Product Management skill**
* Principal Product Managers should possess a unique level of expertise in a critical domain area or product management skill.

**Ability to coach others**
* Principal Product Managers are expected to help raise the bar for the rest of the Product Management team, by a demonstrated ability to coach others in the area of their expertise.

##### Requirements

Beyond the [base requirements](#base-pm-requirements), Principal Product Manager requirements are outlined in the [Product Management Career Development Framework](https://about.gitlab.com/handbook/product/product-manager-role/#product-management-career-development-framework).

### Moving to and moving from

The career paths to and from product management positions are varied, but there are some common patterns.

#### Moving to Product Management

Successful product managers have a passion for solving customer needs. As a result they've
been found to start their journey to product management from the following disciplines:
* **Product Marketing**: Product Marketers with a desire to have more influence in shaping the product often
migrate to the Product Management function.
* **Customer Success**: Customer facing functions like Solutions Architects, Technical Account Managers, and
lead Support techs who've developed a strong understanding of customers' needs in the product often transition
to Product Management out of a desire to shape the direction of the product.
* **UX**: Product Designers and UX leaders have built a great understanding of the personas and pain points their product
targets. A desire to further influence not just the experience but the way the features alleviate those
pain points can draw UX professionals to Product Management.
* **Engineering Manager**: Engineering team leaders who've gained a strong understanding of the customer persona, pain
points and direction of a product often want to further influence that direction by transitioning to Product Management roles.
* **QA/QE**: Quality engineers are responsible for the validation of a product and service. Those who
have a passion for understanding and testing how products are used in the real world by real users often
find a strong alignment to the Product Management function.

#### Moving from Product Management

Within their role, Product Managers are empowered to interact and learn more about functions they are interested in.
Whether that be Marketing, Customer Success, Support, Finance or Engineering - Product Managers are encouraged (and often
required) to understand other functions as part of their daily responsibilities. As such there is plenty of room for
Product Managers to transition to roles outside of the Product Management team. Some of those include:
* **Engineering Leadership**: Product Managers often find a desire to dive deeper into Engineering problems, or grow
mentor and lead engineering teams.
* **Product Marketing**: Product Managers often gravitate to how we target, message and deliver value to our customers.
Product Marketing roles are an excellent way to further that expertise.
* **Customer Success**: Few people know our products as well as Product Managers, and product managers who enjoy directly
solving customers problems on a daily basis make great fits for Customer Success roles.
* **General Management**: As a result of their exposure to a wide variety of functions, Product Managers often make a
transition from Product Management to General Management.

***

## Specialties

### Verify (CI)

We're looking for product managers that can help us work on the future of DevOps tools; specifically, building out continuous integration (CI), code quality analysis, micro-service testing, usability testing, and more.

#### Requirements

- Strong understanding of DevOps, CI/CD, and Release Automation
- Practical understanding of container technologies including Docker and Kubernetes

### Release (CD)

We're looking for product managers that can help us work on the future of DevOps tools; specifically, building out continuous delivery (CD), release orchestration, features flags, and more.

#### Requirements

- Strong understanding of DevOps, CI/CD, and Release Automation
- Understanding of deployment infrastructure and container technologies
- Practical understanding of container technologies including Docker and Kubernetes

### Configure

We're looking for product managers to help us work on the future of DevOps tools; specifically, building out configuration management and other operations-related features such as ChatOps.

#### Requirements

- Strong understanding of CI/CD, configuration management, and operations
- Understanding of deployment infrastructure and container technologies
- Practical understanding of container technologies including Docker and Kubernetes

### Serverless

We're looking for product managers to help us work on the future of DevOps tools; specifically, building out serverless application and function management.

#### Requirements

- Strong understanding of DevOps and cloud-native application development
- Understanding of deployment infrastructure and serverless functions
- Practical understanding of container technologies including Docker and Kubernetes

### Package

We're looking for product managers to cover the [Package stage](/handbook/product/product-categories/#package) of our DevOps lifycle. This candidate will work specifically on building out packaging categories and features such as Docker container registry and binary artifact management.

#### Requirements

- Strong understanding of CI/CD and package management
- Understanding of the complexity of managing multi-artifact application deployment
- Practical understanding of deployment infrastructure and container technologies including Docker and Kubernetes

### Distribution

We're looking for product managers to support our [Distribution group](/handbook/product/product-categories/#admin) and manage our installation, upgrade, and configuration process for our self-managed customers.

#### Requirements

- Strong understanding of system administration
- Practical understanding of deployment infrastructure and container technologies including Docker and Kubernetes

### Secure

We're looking for product managers that can help us work on the future of developer tools; specifically, building out application security testing, including static analysis and dynamic testing.

#### Requirements

- Strong understanding of CI/CD and automated security testing
- Practical understanding of deployment infrastructure and container technologies including Docker and Kubernetes

### Defend

- Strong understanding of SIEM and operational security
- Practical understanding of deployment infrastructure and container technologies including Docker and Kubernetes

### Telemetry

We're looking for product managers that can help us mature our product usage data set to enable better decision making across the company.

#### Requirements

- Strong analytical skills, including an ability to translate business needs back to underlying data sets and structures
- Previous experience with data systems and models

### Growth

We're looking for Product Managers to help us grow our customer base by improving our net new signup flow (Acquisition), trial flow (Conversion), upgrades & seat adds (Expansion), and renewals (Retention).

#### Requirements

- Well versed in experimentation and A/B testing
- Strong analytical skills and a hypothesis driven mindset
- Previous experience leveraging common frameworks and growth techniques

### Fulfillment

We're looking for a PM to ensure an excellent purchase, trial, upgrade, seat addition, and renewal experience for GitLab customers.  This role will involve creating a flexible and powerful billing and licensing system upon which we can build world class customer experiences.  The role will also involve important system level integrations with key 3rd party systems such as Zuora and Stripe.

#### Requirements

- Strong business operations skills and an ability to think systematically about complex workflows and work cross-functionally to ensure an excellent end-to-end customer experience
- Previous experience with high scale billing & licensing systems
- Familiarity with commercial best practices for no touch, sales assisted, and partner assisted transactions
- Excellent communication skills at all levels, including e-group

### Infrastructure

We're looking for a product manager to work closely with our Infrastructure teams, which own the operational aspects of running GitLab.com as well as other services.

#### Requirements

- Deep understanding of the full DevOps lifecycle and tools, with actual experience working in a DevOps role a plus
- Practical understanding of what it takes to run internet applications at scale

#### Responsibilities

- Own prioritization, validation, and scoping of strategic projects and initiatives for the Infrastructure teams
- Champion dogfooding of the GitLab product wherever possible; if the efficacy of dogfooding is in doubt, drive a thoroughly documented build/buy analysis
- Champion cross-product outcomes like fast response times and low operational costs on behalf of the Infrastructure teams
- Ensure the Infrastructure teams are tightly connected in with the mainline Product and Engineering teams

### Search

We're looking for a product manager to make finding things easy and delightful in GitLab. Your goals are to improve the global search experience within GitLab, and ensure the solution can scale from small self-managed instances to GitLab.com.

#### Requirements

- Strong understanding of search engines, algorithms, and workflows. Direct Elasticsearch experience is a plus
- Familiarity with Search workflows, and helping users find what they are looking for
- Understanding of the DevOps lifecycle and tools a plus

### Memory & Database

We're looking for a PM to drive improvements to the responsiveness and scalability of GitLab, and establish best practices for performance oriented development. This role will involve proactively identifying and prioritizing performance hotspots, leading needed architectural changes for additional scalability and efficiency, and delivering improvements to our development process for writing performant code.

#### Requirements

- An understanding of how complex applications function, and an ability to think systematically about application flows
- Strong analytical skills, and a quantitative approach to incremental performance improvements
- Hands-on experience with databases, caching strategies, and web applications

## Relevant links

- [Product Handbook](/handbook/product)
- [Engineering Workflow](/handbook/engineering/workflow)
