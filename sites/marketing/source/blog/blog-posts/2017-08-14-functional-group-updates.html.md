---
title: "GitLab's Functional Group Updates: July 31st - August 10th" # replace "MMM" with the current month, and "DD-DD" with the date range
author: Chloe Whitestone
author_gitlab: chloemw
author_twitter: drachanya
categories: company
image_title: '/images/blogimages/functional-group-update-blog-cover.jpg'
description: "The Functional Groups at GitLab give an update on what they've been working on from July 31st - August 10th"
tags: inside GitLab, functional group updates
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Thursday, right before our [GitLab Team call](/handbook/#team-call), a different Functional Group gives an [update](/handbook/people-operations/group-conversations/) to our team.

The format of these calls is simple and short where they can either give a presentation or quickly walk the team through their agenda.

<!-- more -->

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Backend Team

[Presentation slides](https://docs.google.com/presentation/d/16bUZHzqC2YC_8GrTdjQS-OHhmfyKhvEW38PnPTmiN10/edit)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/BjpDXnvHSlU" frameborder="0" allowfullscreen="true"></iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### UX Team

[Presentation slides](https://docs.google.com/a/gitlab.com/presentation/d/1UxlazI5LQeWTJbbL_pkRaqHU8eV0_Qo1LEdA8r-DxV4/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/h-tUxJjvdOM" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Frontend Team

[Presentation slides](https://docs.google.com/a/gitlab.com/presentation/d/145VQTpdxMZpnR-GSFk-Qs4QqfhgnUxYlMP5OwJc0QCw/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/GAZSjpDVm0E" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Build Team

[Presentation slides](https://docs.google.com/presentation/d/1ts8knff_f2_diNjeX7sovUC361wib2HiXL2hZz5wuuU/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/MtkievTt5e0" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Support Team

[Presentation slides](https://docs.google.com/presentation/d/1EizMPiTJFYm7R7Av6J7DguR_Crgo_t8pufLYKoGC5sU/)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/a4gYeTnOShM" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!
