require 'spec_helper'

describe Gitlab::CodeOwners::OwnerValidator do
  subject(:validator) { described_class.new(data) }

  before do
    allow(YAML).to receive(:load_file) { [{ 'gitlab' => 'User' }] }
  end

  let(:data) { { '.' => '@user', 'source' => '@user' } }

  describe '#valid?' do
    subject { validator.valid? }

    it 'returns true' do
      is_expected.to be_truthy
    end

    context 'when owner looks like a group' do
      let(:data) { { '.' => '@gitlab-team' } }

      it 'returns true' do
        is_expected.to be_truthy
      end
    end

    context 'when owner is a group' do
      let(:data) { { '.' => '@timtams', 'source' => '@gl-product-leadership' } }

      it 'returns true' do
        is_expected.to be_truthy
      end
    end

    context 'when owner is not found' do
      let(:data) { { '.' => '@user1' } }

      it 'returns false' do
        is_expected.to be_falsey
      end

      it 'returns errors' do
        subject

        expect(validator.errors.count).to eq(1)
        expect(validator.errors.first).to include('Incorrect owner: @user1')
      end
    end
  end
end
