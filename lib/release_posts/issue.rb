# frozen_string_literal: true

require 'base64'
require 'uri'

require_relative '../category'
require_relative '../group'
require_relative '../stage'

module ReleasePosts
  class Issue
    include Helpers

    CONTENT_REGEX = /^[#]+ (Release notes)\W*^(?<description>.*?)\s*(^[#]+ |\Z)/im
    IMAGE_REGEX = /\!\[.*?\]\((?<image>.*?)\)/im
    DOCS_PREFIX = 'https://docs.gitlab.com/'

    def initialize(issue, type_name)
      @issue = issue
      @type_name = type_name
    end

    def iid
      @issue['iid']
    end

    def project_id
      @issue['project_id']
    end

    def group_id
      @issue['group_id']
    end

    def title
      @issue['title']
    end

    def description
      @issue['description']
    end

    def labels
      @issue['labels']
    end

    def milestone_id
      @issue.dig('milestone', 'iid')
    end

    def milestone_title
      @issue.dig('milestone', 'title')
    end

    def web_url
      @issue['web_url']
    end

    def epic?
      project_id.nil?
    end

    def issue_web_url
      web_url unless epic?
    end

    def epic_web_url
      web_url if epic?
    end

    def due_date
      d = @issue['due_date'] || @issue.dig('milestone', 'due_date') || '2019-04-22'
      Date.iso8601(d).strftime("%B %-d, %Y")
    end

    def documentation_url
      placeholder = +'https://docs.gitlab.com/ee/#amazing'
      URI.extract(raw_content).find { |u| u.start_with? DOCS_PREFIX } || placeholder
    end

    def assignees
      @issue['assignees']&.map { |a| a['username'] }
    end

    def group_label
      labels.find { |label| label.start_with? GROUP_LABEL_PREFIX }
    end

    def group
      return nil if group_label.nil?

      normalized = group_label.delete_prefix(GROUP_LABEL_PREFIX).tr(' ', '_')
      Gitlab::Homepage::Group.all!.find { |g| g.key == normalized }
    end

    def stage_label
      labels.find { |label| label.start_with? STAGE_LABEL_PREFIX }
    end

    def stage
      return nil if stage_label.nil?

      normalized = stage_label.delete_prefix(STAGE_LABEL_PREFIX).tr(' ', '_')
      Gitlab::Homepage::Stage.all!.find { |s| s.key == normalized }
    end

    def category_labels
      labels.select { |label| label.start_with? CATEGORY_LABEL_PREFIX }
    end

    def categories
      return nil if category_labels.nil?

      normalized = category_labels.map { |label| label.delete_prefix(CATEGORY_LABEL_PREFIX).downcase }
      Gitlab::Homepage::Category.all!.filter { |category| normalized.include? category.name.downcase }
    end

    def available_in
      available_in = ['ultimate']
      return available_in if labels.include? 'GitLab Ultimate'

      available_in.unshift('premium')
      return available_in if labels.include? 'GitLab Premium'

      available_in.unshift('starter')
      return available_in if labels.include? 'GitLab Starter'

      available_in.unshift('core')
    end

    def release_post_label
      labels.find { |label| label.start_with? RP_LABEL_PREFIX }
    end

    def type_name
      return @type_name if @type_name

      type_found = TYPES.find do |type|
        type.label == release_post_label
      end

      type_found ? type_found.name : nil
    end

    def slug
      slugify("#{stage.key}_#{group.key}_#{title}")
    end

    def content
      content = raw_content

      # Remove the lines defining the documentation and image
      [documentation_url, image_url].compact.each do |u|
        content = content.each_line.reject { |line| line.include? u }.join
      end

      +"#{content.strip}\n"
    end

    def image_url
      return unless matches = raw_content.match(IMAGE_REGEX)

      matches[:image].to_s
    end

    def image_binary
      u = URI(web_url.split('/-/')[0])
      u.path += image_url

      URI.open(u).read
    end

    def image_base64
      Base64.strict_encode64(image_binary)
    end

    def release_post_workflow_label
      labels.find { |label| label.start_with? RELEASE_POST_WORKFLOW_PREFIX }
    end

    private

    def raw_content
      placeholder = +"Lorem ipsum [dolor sit amet](#link), consectetur adipisicing elit. Perferendis nisi vitae quod ipsum saepe cumque quia `veritatis`.\n"
      match = description.match(CONTENT_REGEX)

      return placeholder unless match

      match[:description]
    end

    def titleize(string)
      string.split.each(&:capitalize!).join(' ').to_s
    end

    def slugify(value)
      value.strip.gsub(/\s/, '_').gsub(/\W/, '').downcase
    end
  end
end
