- name: Net New Business Pipeline Created
  base_path: "/handbook/marketing/performance-indicators/"
  definition: The combined IACV of the net "New Business" opportunities. This data
    comes from a sheetload file.
  target: vs Plan > 1
  org: Marketing
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - tbd
  sisense_data:
    chart: 6592070
    dashboard: 431555
    embed: v2
- name: Pipe-to-spend per marketing activity
  base_path: "/handbook/marketing/performance-indicators/"
  definition: Pipeline (IACV on Opportunities) divided by the total marketing program
    spend, broken down by marketing activity.
  target: 5
  org: Marketing
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - tbd
- name: Marketing efficiency ratio
  base_path: "/handbook/marketing/performance-indicators/"
  definition: Marketing efficiency for a given month is calculated as the ratio of
    IACV from closed won opportunities for the current month and the 2 preceding months
    vs the Marketing Operating Expenses (IACV / Marketing Operating Expenses) for
    the same time period. GitLab's target is greater than 2.
  target: 2
  org: Marketing
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - tbd
- name: Opportunities per XDR per month created
  base_path: "/handbook/marketing/performance-indicators/"
  definition: The net number of unique opportunities that are created and have a "sales
    development representative" assigned/attached to the opportunity segmented by
    xDR and by month. This data comes from Salesforce.
  target: TBD
  org: Marketing
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - tbd
  sisense_data:
    chart: 6222245
    dashboard: 431555
    embed: v2
- name: LTV / CAC ratio
  base_path: "/handbook/marketing/performance-indicators/"
  definition: The customer Life-Time Value to Customer Acquisition Cost ratio <a href="https://about.gitlab.com/handbook/sales/#ltv">LTV</a>:<a
    href="https://about.gitlab.com/handbook/sales/#customer-acquisition-cost-cac">CAC</a>
    measures the relationship between the lifetime value of a customer and the cost
    of acquiring that customer. <a href="https://www.klipfolio.com/resources/kpi-examples/saas-metrics/customer-lifetime-value-to-customer-acquisition-ratio">A
    good LTV to CAC ratio is considered to be > 3.0.</a>
  target: greater than 3.0
  org: Marketing
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - tbd
- name: Social Media Followers
  base_path: "/handbook/marketing/performance-indicators/"
  definition: Follower growth across social channels is a good indicator or resonating
    with a wider audience or deeply connecting within a niche. Follower growth is
    defined as all net-new followers, across all GitLab-branded social media channels
    in a given period. This KPI is tracked by exporting reports and data from our
    social media management tool, Sprout Social. This data is then manually ingested
    into Sisense via a formatted Google Sheet. This KPI will evolve further as more
    data becomes available.
  target: 0
  org: Marketing
  is_key: true
  public: true
  health:
    level: 3
    reasons:
    - Currently, there is no target because we are in a benchmarking year and without
      previous strategic data.
  sisense_data:
    chart: 8146731
    dashboard: 621921
    embed: v2
- name: New Unique Web Visitors (about.gitlab.com)
  base_path: "/handbook/marketing/performance-indicators/"
  definition: New Users in Google Analytics, or the number of first-time users during
    the selected date range. <b>Note</b>, there is ongoing work in finalizing this
    KPI chart in Sisense (WIP); please reference the DataStudio chart, which is also
    accessible by the url below <iframe width="600" height="338" src="https://datastudio.google.com/embed/reporting/12FyakX5mE3BTqXO3QgniMNq0KP4LjyNG/page/JcPY"
    frameborder="0" style="border:0" allowfullscreen></iframe> <br />
  org: Marketing
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - We need to define if this includes docs, about, and forum or only uses about.gitlab.com
      subdomain. Then we need to set a target, preferable a year over year target
      to account for monthly seasonality.
  sisense_data:
    chart: 8490725
    dashboard: 431555
    embed: v2
  urls:
  - https://datastudio.google.com/embed/reporting/12FyakX5mE3BTqXO3QgniMNq0KP4LjyNG/page/JcPY
- name: Total Web Sessions (about.gitlab.com)
  base_path: "/handbook/marketing/performance-indicators/"
  definition: Total number of sessions in Google Analytics within the selected date
    range. These are visits to the web site that may include multiple pages. <b>Note</b>,there
    is ongoing work in finalizing this KPI chart in Sisense (WIP); please reference
    the DataStudio chart, which is also accessible by the url below <iframe width="600"
    height="338" src="https://datastudio.google.com/embed/reporting/1ycD6QqPYpm1bYAYoaeDTnIPZNrXI53zw/page/JcPY"
    frameborder="0" style="border:0" allowfullscreen></iframe> <br />
  org: Marketing
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - We need to define if this includes docs, about, and forum or only uses about.gitlab.com
      subdomain. Then we need to set a target, preferable a year over year target
      to account for monthly seasonality.
  sisense_data:
    chart: 8490690
    dashboard: 431555
    embed: v2
  urls:
  - https://datastudio.google.com/embed/reporting/1ycD6QqPYpm1bYAYoaeDTnIPZNrXI53zw/page/JcPY
- name: Meetups per month
  base_path: "/handbook/marketing/performance-indicators/"
  definition: We aim to participate in at least 20 GitLab Meetups per month. A GitLab
    Meetup is defined as a meetup with a presentation given by a GitLab team member
    or a wider community member about GitLab or a tangential topic. It does not include
    meetups without GitLab content where GitLab only provides support. This KPI is
    tracked by counting the number of issues in the GitLab.com data with the <a href="https://gitlab.com/groups/gitlab-com/marketing/-/boards/962542?label_name[]=Meetups">Meetups
    label</a> in the Marketing group. Each event should have one associated issue
    with the `Meetups` label. The issue due date should be set to the event date.
    The combination of `Meetups` label and due date will be used by our data dashboard
    to count the number of meetups each month. This method of tracking can results
    in errors when meetup issues are created in other groups or when GitLab-related
    presentations at meetups are not shared with the Community Relations team.
  target: 20
  org: Marketing
  is_key: true
  public: true
  health:
    level: 1
    reasons:
    - GitLab is not currently supporting in-person meetups in order to encourage responsible
      physical distancing within our community. We have increased efforts to organize
      virtual events.
  sisense_data:
    chart: 7799761
    dashboard: 431555
    embed: v2
- name: Wider Community merged MRs per release
  base_path: "/handbook/marketing/performance-indicators/"
  definition: The Wider community contributions per milestone metric reflects the
    number of merge requests (MRs) merged from the wider GitLab community for each
    milestone from our GitLab.com data. To count as a contribution the MR must have
    been merged, have the <a href="https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Community%20contribution">Community
    contribution</a> label, have a GitLab milestone set (e.g `11.11`, `12.0`, etc.)
    and be against one of the <a href="https://about.gitlab.com/handbook/marketing/community-relations/code-contributor-program/#monitored-projects">monitored
    gitlab-org group projects</a>. <b>Note</b>, Wider community metrics are more reliable
    after 2015 when the Community contribution label was created, and lastly, there
    is ongoing work to provide deeper insights into this metric in the <a href="https://app.periscopedata.com/app/gitlab/593556/Wider-Community-Dashboard">Wider
    Community Sisense dashboard</a>. Previously, this KPI was represented by the below
    Bitergia chart.  <iframe src="https://gitlab.biterg.io/app/kibana#/dashboard/465b66f0-882a-11e9-b37c-9d3431060b53?embed=true&_g=(refreshInterval%3A(display%3AOff%2Cpause%3A!f%2Cvalue%3A0)%2Ctime%3A(from%3Anow-10y%2Cmode%3Arelative%2Cto%3Anow))"
    height="600" width="800"></iframe><br/>
  target: TBD
  org: Marketing
  is_key: true
  public: true
  health:
    level: 2
    reasons:
    - There is a seasonality to wider community contributions (e.g. lower activities
      during holiday periods).
  sisense_data:
    chart: 7765561
    dashboard: 593556
    embed: v2
- name: Community Channel Response Time
  base_path: "/handbook/marketing/performance-indicators/"
  definition: Community Channel Response Time is the time between an inbound message
    and the first-reply time. The current goal is to be under 7 hours for all channels
    and under 5 hours for high-priority channels. This response time is currently
    tracked in Zendesk.
  org: Marketing
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - The Marketing team is working with the Data Team <a href="https://gitlab.com/gitlab-data/analytics/-/issues/3771">to
      get the data into the data warehouse</a>.
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/3771
- name: New hire location factor - Marketing
  base_path: "/handbook/marketing/performance-indicators/"
  parent: "/handbook/hiring/metrics/#new-hire-location-factor"
  definition: The average location factor of all newly hired team members within the
    last rolling 3 month as of the end of the period. (eg. If the current month is
    2019-09-01 then the calculation pulls months 2019-06-01 to 2019-08-31). Each division
    and department has their own new hire location factor target.
  target: "< 0.72"
  org: Marketing
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - tbd
  sisense_data:
    chart: 6786698
    dashboard: 431555
    embed: v2
- name: Pipeline coverage
  base_path: "/handbook/marketing/performance-indicators/"
  definition: Pipeline with close dates in a given period (quarter) divided by IACV
    target. Pipeline coverage should be 2X for current quarter, 1X for next quarter,
    and .5 for 2 QTRs out.
  target: 2X for current quarter, 1X for next quarter, and .5 for 2 QTRs out
  org: Marketing
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - tbd
- name: Lead follow-up response time
  base_path: "/handbook/marketing/performance-indicators/"
  definition: The amount of time (Days, hours, minutes) it takes for a Lead/Contact
    to move from MQL to another status - this indicates how long it took for a sales
    rep/xDR to respond to the record becoming a MQL.
  org: Marketing
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - tbd
- name: Qty. of Case Studies published per month
  base_path: "/handbook/marketing/performance-indicators/"
  org: Marketing
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - tbd
- name: 50% or more SOV compared to most published competitor
  base_path: "/handbook/marketing/performance-indicators/"
  definition: The comparison of the GitLab share of voice (SOV) percentage to most
    published competitors’ SOV percentage. SOV is the number of media mentions compared
    to the media mentions received by a competitor and then the percentage is calculated
    out of 100%. The SOV percentages are tracked via a media service called TrendKite
    by our PR agency.
  target: greater than 50%
  org: Marketing
  is_key: true
  public: true
  health:
    level: 2
    reasons:
    - We use TrendKite to track SOV and do not have ability to share public dashboard.
- name: Increase partner focused media coverage by 25% in FY21
  base_path: "/handbook/marketing/performance-indicators/"
  definition: The amount of media coverage/articles focused on partner and channel
    related topics in FY21 compared to FY20. Increasing the percentage of coverage
    increases GitLab’s brand awareness on the specific topic.
  target: greater than 25% in FY21
  org: Marketing
  is_key: true
  public: true
  health:
    level: 2
    reasons:
    - Do not have a public dashboard for coverage numbers.
- name: Increase amount of remote work focused media coverage by 50% in FY21 over
    FY20
  base_path: "/handbook/marketing/performance-indicators/"
  definition: The amount of media coverage/articles focused on remote work topics
    in FY21 compared to FY20. Increasing the percentage of coverage increases GitLab’s
    brand awareness on the specific topic.
  target: greater than 50% in FY21 over FY20
  org: Marketing
  is_key: true
  public: true
  health:
    level: 2
    reasons:
    - Do not have a public dashboard for coverage numbers.
- name: Total number of MQLs by month
  base_path: "/handbook/marketing/performance-indicators/"
  definition: Total number of <a href="https://about.gitlab.com/handbook/business-ops/resources/#sts=MQL%20Definition">Marketo
    Qualified</a> Leads per month. This data comes from a sheetload file.
  target: TBD
  org: Marketing
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - tbd
  sisense_data:
    chart: 6592066
    dashboard: 431555
    embed: v2
- name: Product Downloads
  base_path: "/handbook/marketing/performance-indicators/"
  definition: Total downloads by installation method (Omnibus, Cloud native helm chart,
    Source, etc). This chart is populated with the versions.gitlab.com data.
  target: TBD
  org: Marketing
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - tbd
  sisense_data:
    chart: 5542406
    dashboard: 428908
