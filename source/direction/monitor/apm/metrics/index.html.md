---
layout: markdown_page
title: "Category Direction - Metrics"
description: "Metrics help users understand how their applications are performing, and if they are healthy. Find more information here!"
canonical_path: "/direction/monitor/apm/metrics/"
---

- TOC
{:toc}

## Metrics

### Introduction and how you can help
Thanks for visiting this category strategy page on Metrics in GitLab. This category belongs to and is maintained by the [Health](/handbook/engineering/development/ops/monitor/health/) group of the Monitor stage.

This page is maintained by [Kevin Chu](https://gitlab.com/kbychu), group product manager. You can connect with him via [Zoom](https://calend.ly/kchu-gitlab) or [Email](mailto:kchu@gitlab.com).If you're a GitLab user and have direct knowledge of your Metrics usage, we'd especially love to hear your use case(s).

## What are metrics

Metrics help users understand the health and status of your services and essential for ensuring the reliability and stability of those services., metrics normally represents by raw measurements of resource usage over time (e.g. measure memory usage every 10 second). Some metrics represent the status of an operating system (CPU, memory usage). Other types of data tied to the specific functionality of a component (requests per second, latency or error rates).
The most straightforward metrics, to begin with, are those already exposed by your operating system hence easier to collect (e.g. Kubernetes metrics).
For other components, especially your applications, you may have to add code or interfaces to expose the metrics you care about. Exposing metrics is sometimes known as [instrumentation](https://about.gitlab.com/direction/monitor/apm/workflow/instrument/), the collection of metrics from an end point is called scraping.

## Our mission

Provide users with information about the health and performance of their infrastructure, applications, and system for insights into reliability, stability, and performance.

## Target audience

Metrics are essential for all users across the DevOps spectrum. From developers who should understand the performance impact of changes they are making, as well as operators responsible for keeping production services online.

Our vision is that in 1-2 years, GitLab metrics is the main day-to-day tool for monitoring cloud-native applications for SMBs.

We are targeting [Software Developer](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer) and [DevOps Engineer](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer) working in a SMB from the following reasons:

* SMBs are more likely to be cloud-native - no doubt that the trend for large enterprises is going cloud-native. However, they will remain hybrid and need a full-blown monitoring solution to cover both approaches.
* Cloud-native is one of the [fastest-growing markets in IT](https://www.gartner.com/smarterwithgartner/cloud-shift-impacts-all-it-markets), given how GitLab is positioned in this space going after these segment will allow us to gain market share and quickly expand.
* Developers in this market segment are more likely to be a complete DevOps engineer, responsible for development, deployment and monitoring.
* SMBs are less willing to spend their budget on expensive monitoring systems - our base monitoring features are free for all GitLab users.
* This strategy means that it is less likely we'll compete head-on with the established vendors there are in the market today.

Since the team's current focus is [dogfooding metrics](/direction/monitor/apm/metrics/#dogfooding-metrics), our immediate  target audience is the [GitLab Infrastructure Team](/handbook/engineering/infrastructure/). We plan to build the minimal work needed for them to start dogfooding our metrics dashboard before shifting focus to consider the overall needs of the target audience mentioned above.

## Current experience

The experience today offers you to deploy Prometheus instance into a project cluster by a push of a button, the Prometheus instance will run as a GitLab managed application. Once deployed, it will automatically collect key metrics from the running application which are displayed on an out of the box [dashboard](https://docs.gitlab.com/ee/user/project/integrations/prometheus.html#using-the-metrics-dashboard). Our dashboards provide you with the needed [flexability](https://docs.gitlab.com/ee/user/project/integrations/prometheus.html#defining-custom-dashboards-per-project) to display any metric you desire you can set up alerts, configure [variables](https://docs.gitlab.com/ee/user/project/integrations/prometheus.html#templating-variables-for-metrics-dashboards) on a generic dashboard, drill into the [relavant logs](https://docs.gitlab.com/ee/user/project/integrations/prometheus.html#view-logs) to troubleshoot your service and more...
If you already have a running Prometheus deploy into your cluster simply [connect](https://docs.gitlab.com/ee/user/project/integrations/prometheus.html#external-prometheus-instances) it to your GitLab and start using our GitLab metrics dashboard.

## Targeted workflows

The target workflow, listed below, is our [high-level roadmap](https://app.mural.co/t/gitlab2474/m/gitlab2474/1589786898353/a20f7938166fd7bc444b8c9cb17f447fcbe5f577). It is based on competitive analysis, user research, and customer interviews. The details of each workflow are listed in the epics and issues.

<iframe width="560" height="315" src="https://www.youtube.com/embed/dUFHYjbIsG8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Getting data in

The first step in application performance management is collecting the proper measurements or telemetry data. Instrumenting critical areas and reporting metrics of your system are prerequisites to understanding the health and performance of your services and application. Our metric solution is powered by Prometheus, targeting users of Kubernetes. We need to make sure our users can successfully
1. Enable Prometheus for a Kubernetes cluster - Whether you have a preinstalled Prometheus instance on your cluster that is linked to GitLab, or you'd like us to deploy Prometheus for you.
2. Deploy exporters and instrument app - We should help you as much as possible with instrumenting your applications and deploying exporters into a cluster, so that you can leverage all of telemetry data from across your system.
3. Enable metrics scraping - We need an easy way to enable metrics scraping. When possible, we should strive to do this automatically.

#### Supported research and epic
- [Instrument Prometheus metrics](https://gitlab.com/gitlab-org/ux-research/-/issues/553)
- [Instrument to viable](https://gitlab.com/groups/gitlab-org/-/epics/2316)


### See metric in a dashboard

Once you've collected a set of metrics, the next step is to see those metrics in a dashboard.

1. Out of the box dashboard (OOTB) - Everyone loves beautiful dashboards, however building one is a challenge; the more useful, OOTB dashboard we can provide, the better we can help users get started quickly. Established vendors take pride in the amount of OOTB dashboard they can provide to their customers. We could do them same and leverage GitLab community with a growing library of dashboards that anyone can contribute to.
2. Workflow enablement (Dogfooding) - Connecting data from panels and dashboards together can help operators detect and diagnose problems. Solutions such as annotations, templating, drilldowns and improved visuaization are part of the toolkit in mature solutions. This area is where most of our dogfooding metrics effort is focused on today.
3. Customize dashboard and add dashboards - Adding a metric to a dashboard and adding new dashboards are basic functionalities a monitoring solution should have. Today, this can be quite confusing for a first time GitLab Metrics user. We intend to make this step easy and discoverable for our users. Doing so will help us increase user adoption.

#### Supported research and epic
- [What types of dashboards we should build for our users](https://gitlab.com/gitlab-org/ux-research/-/issues/533)
- [Understand how our users use Grafana](https://gitlab.com/gitlab-org/ux-research/-/issues/748)
- [Add metric to a Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2863)
- [Dogfooding metrics](https://gitlab.com/gitlab-org/monitor/apm/-/issues/28)
- [What dashboard features should we build next?](https://gitlab.com/gitlab-org/ux-research/-/issues/882)

### Alerting
Users need to be alerted on any threshold violation, ideally before their end-users
1. Setting up an alert - Should be a straight forward action supported by all metrics and most of the chart types.

### Kubernetes monitoring
Our vision is that in 1-2 years, GitLab metrics will be the primary day-to-day tool for monitoring cloud-native applications. To achieve that we would need to support:
1. Cluster insight - Provide our users insight to clusters, pods and nodes.
2. Key metrics and OOTB dashboard - Curated and scalable dashboards that provide out of the box k8s metrics and bird's eye view across environments.

#### Supported research and epic
- [Typical workloads and metrics to collect on Kubernetes deployments](https://gitlab.com/gitlab-org/ux-research/-/issues/389)
- [Infrastracture dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2135)

### Log aggregation

In the distributed nature of cloud-native applications, it is crucial and critical to collect logs across multiple services and infrastructure, present them in an aggregated view, so users could quickly search through a list of logs that originate from multiple pods and containers. Metrics and logs are related and we intend to make the correlation for users so that they can more quickly get to the answers they are looking for. You can review our [logging direction page](logging) for more information.

## What's Next & Why

### Dogfooding metrics

We are actively [Dogfooding](https://about.gitlab.com/handbook/product/product-processes/#when-do-we-dogfood) GitLab Metrics with the [Infrastructure team](https://about.gitlab.com/handbook/engineering/infrastructure/team/) to migrate dashboards used for monitoring Gitlab.com from Grafana to GitLab Metrics. This will help us receive rapid feedback as we mature metrics.
In terms of this overall roles and responsibilities:

- The APM group is the DRI for priortizing and building the features the Infrastructure team needs in order to use the GitLab metrics dashboard for monitoring gitlab SaaS.
- The Infrastructure team is the DRI for building and managing GitLab metrics dashboards, identifying and sharing gaps found while dogfooding, and for redirecting users away from their Grafana dashboards.

We will iterate based on the following process:

1. Identify key Grafana dashboards to clone into GitLab metrics dashboards - DRI Infra team
1. Cloning Grafana dashboards to a GitLab project as GitLab metrics dashboards to a [testbed project](https://gitlab.com/gitlab-org/monitor/sandbox/test-metrics-dashboard) - DRI APM team
1. Use GitLab metrics dashboard and provide feedback (in simulation day or after completing triaging an incident) - DRI Infra team
1. Prioritize and implement issues based on feedback or find workarounds - DRI APM team
1. At a certain point in time (which agreed by both teams) the infra team will become the DRI for those GitLab metrics dashboards and clone it to their project
1. Turn off the original Grafana dashboard - the Infra team is the only DRI for making this decision, this is an independent decision that is not based only on the implementation issues.

| Grafana | Testbed | Handbook Reference | Status |
|---|---|---|---|
| [General SLAs](https://dashboards.gitlab.com/d/general-slas/general-slas?orgId=1) | [https://gitlab.com/gitlab-org/monitor/sandbox/test-metrics-dashboard/-/environments/2115574/metrics?dashboard=.gitlab%252Fdashboards%252Fgeneral-slas.yml](https://gitlab.com/gitlab-org/monitor/sandbox/test-metrics-dashboard/-/environments/2115574/metrics?dashboard=.gitlab%252Fdashboards%252Fgeneral-slas.yml) | [Tools for Engineers SLA Dashboard link](/handbook/engineering/development/processes/Infra-Dev-Escalation/process.html#tools-for-engineers) | Awaiting Feedback |
| [Public Dashboard Landing Page](https://dashboards.gitlab.com/d/GTp20b1Zk/public-dashboard-splashscreen?orgId=1) | [https://gitlab.com/gitlab-org/monitor/sandbox/test-metrics-dashboard/-/environments/2115574/metrics?dashboard=.gitlab%252Fdashboards%252Fpublic-dashboard-splash-screen.yml](https://gitlab.com/gitlab-org/monitor/sandbox/test-metrics-dashboard/-/environments/2115574/metrics?dashboard=.gitlab%252Fdashboards%252Fpublic-dashboard-splash-screen.yml) | [Infrastructure KPI 6 image link](/handbook/business-ops/data-team/kpi-index/#infrastructure-department-kpis) | Awaiting Feedback |
| [Cloudflare Traffic Overview](https://dashboards.gitlab.com/d/sPqgMv9Zk/cloudflare-traffic-overview?orgId=1) | [https://gitlab.com/gitlab-org/monitor/sandbox/test-metrics-dashboard/-/environments/2115574/metrics?dashboard=.gitlab%252Fdashboards%252Fcloudflare-traffic-overview.yml](https://gitlab.com/gitlab-org/monitor/sandbox/test-metrics-dashboard/-/environments/2115574/metrics?dashboard=.gitlab%252Fdashboards%252Fcloudflare-traffic-overview.yml) | N/A | Awaiting Feedback |
| [Logging](https://dashboards.gitlab.com/d/USVj3qHmk/logging?orgId=1) | [https://gitlab.com/gitlab-org/monitor/sandbox/test-metrics-dashboard/-/environments/2115574/metrics?dashboard=.gitlab%252Fdashboards%252Flogging.yml](https://gitlab.com/gitlab-org/monitor/sandbox/test-metrics-dashboard/-/environments/2115574/metrics?dashboard=.gitlab%252Fdashboards%252Flogging.yml) | N/A | Awaiting Feedback |

### Customize dashboard and add dashboards

As mentioned above, adding a metric to a dashboard and adding new dashboards are basic functionalities a monitoring solution should have. Today, this can be quite confusing for a first time GitLab Metrics user. We are actively working on improving these workflows and allow a better onboarding experience for our users. Detailed information can is available in the following:

* [Add metric epic](https://gitlab.com/groups/gitlab-org/-/epics/2863)
* [Dashboard actions](https://gitlab.com/gitlab-org/gitlab/-/issues/223204)
