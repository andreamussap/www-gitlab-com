---
layout: markdown_page
title: "Category Direction - Tracing"
description: "Tracing is a part of Application Performance Monitoring (APM) solution, and described as a way to measure an application while running. Learn more!"
canonical_path: "/direction/monitor/apm/tracing/"
---

- TOC
{:toc}

# Tracing

## Introduction and how you can help
Thanks for visiting this category strategy page on Tracing in GitLab. This category belongs to and is maintained by the [Health](/handbook/engineering/development/ops/monitor/health/) group of the Monitor stage.

This page is maintained by [Kevin Chu](https://gitlab.com/kbychu), group product manager. You can connect with him via [Zoom](https://calend.ly/kchu-gitlab) or [Email](mailto:kchu@gitlab.com). If you're a GitLab user and have direct knowledge of your Metrics usage, we'd especially love to hear your use case(s)  

## Background 

Tracing is a part of Application Performance Monitoring (APM) solution, and described as a way to measure an application while running. To enable tracing, code instrumentation is required. With it, it is possible to gain in-depth insight into the app across all layers by measuring the execution time of a user journey. Tracing information can be used to troubleshoot an application by measuring different metrics of Traces and Spans.
*  **Trace** - represents an application processing a request (e.g., verifying login credential)
*  **Span** - represents a single unit of work from start to finish  

A trace is normally constructed out of multiple spans.  

With the rise of microservices applications, it has become difficult to correlates traces and spans, as those are likely to be routed across distributed components.  **Distributed tracing** is a method that correlates multiple Traces and Spans of a single transaction across different components. Making it easier to understand the path a transaction took through multiple services.

## Current status

Gitlab integrates with [Jaeger](https://www.jaegertracing.io/) - an open-source, end-to-end distributed tracing system tool used for monitoring and troubleshooting microservices-based distributed systems. If you are using Jaeger it is possible to view its UI within Gitlab.

## What's Next & Why
We're pursuing continued iteration on our initial MVP via the [tracing to viable](https://gitlab.com/groups/gitlab-org/-/epics/2348) epic with a specific next step to [enable deployment of Jaeger to your Kubernetes cluster](https://gitlab.com/gitlab-org/gitlab-ee/issues/5182) with a push of a button. 

## Maturity Plan
* [Base Epic](https://gitlab.com/groups/gitlab-org/-/epics/837)
* [Tracing to Viable](https://gitlab.com/groups/gitlab-org/-/epics/837)
