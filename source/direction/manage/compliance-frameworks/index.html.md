---
layout: markdown_page
title: "Category Direction - Compliance Frameworks"
description: "Part of managing compliance is knowing what rules need to be in place for specific areas or functions of the business. Learn more!"
canonical_path: "/direction/manage/compliance-frameworks/"
---

- TOC
{:toc}

Last Reviewed: 2020-02-07

## Compliance Frameworks

Thanks for visiting this direction page on Compliance Frameworks in GitLab. If you'd like to provide feedback on this page or contribute to this vision, please feel free to open a merge request for this page or comment in the [corresponding epic](https://gitlab.com/groups/gitlab-org/-/epics/2302) for this category.

## Problem to solve
An organization using GitLab needs to ensure their users are complying with the legal and regulatory requirements that govern their industry. Part of managing this compliance is knowing what rules need to be in place for specific areas or functions of the business.

Currently, there's no way for an organization to know, within GitLab, what groups or projects are subject to particular compliance requirements.

* What success looks like: GitLab administrators and group owners should be able to associate groups and projects with specific compliance frameworks, such as SOC 2, GDPR, HIPAA, SOX, etc. This association should be tied to certain [Compliance Controls](https://about.gitlab.com/direction/manage/compliance-management/) that govern how the groups and projects operate.

## Maturity
Compliance Frameworks is currently in the **planned** state. This is because we don't yet have a way to associate groups and projects with specific Compliance Frameworks.

In order to bring Compliance Frameworks to the **viable** state, we will be implementing features that allow GitLab group owners and administrators to assign specific [Compliance Controls](https://about.gitlab.com/direction/manage/compliance-management/) to projects. These controls should introduce simple, but meaningful controls to govern activity within a project, such as ensuring merge request approval rules are adhered to and cannot be bypassed without explicit approval.

## What's Next & Why

We'll be iterating on Compliance Frameworks to incorporate additional requirements from SOC 2, GDPR, PCI-DSS, HIPAA, SOX, and more.

We'd like to leverage the [GitLab Control Framework](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/compliance.html#gitlabs-control-framework-gcf) as a single source of truth. By using the GCF, a group owner or administrator could apply specific GCF controls to a project to support multiple legal and regulatory frameworks as opposed to requiring separate framework assignments. 

Examples of the GCF controls:

* [CM.1.02 - Change Approval](https://about.gitlab.com/handbook/engineering/security/guidance/CM.1.02_change_approval.html) - Ensures things like MR approval settings are set and non-admins or group owners cannot make changes to these settings.
* [RM.1.01 - Risk Assessment](https://about.gitlab.com/handbook/engineering/security/guidance/RM.1.01_risk_assessment.html) - Could auto-generate an issue for each authored MR with a template for assessing the risk and impact of the desired change.

We're currently exploring the GCF as a mapping precedent in this [discovery vision issue](https://gitlab.com/gitlab-org/gitlab/issues/40600) and we encourage you to provide your feedback.

## How you can help
This vision is a work in progress, and everyone can contribute:

* Please comment and contribute in the linked issues and epics on this category page. Sharing your feedback directly on GitLab.com is the best way to contribute to our vision.
* Please share feedback directly via email, Twitter, or on a video call. If you’re a GitLab user and have direct knowledge of your need for compliance and auditing, we’d especially love to hear from you.
* Join our [Compliance Special Interest Group (SIG)](https://gitlab.com/gitlab-org/ux-research/issues/532) where you have a direct line of communication with the PM for Manage:Compliance
