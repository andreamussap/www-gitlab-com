---
layout: markdown_page
title: "Category Direction - Importers"
description: "This GitLab group is focused on enabling GitLab.com adoption through the introduction of group import/export. Find more information here!"
canonical_path: "/direction/manage/importers/"
---

- TOC
{:toc}

Last Reviewed: 2020-08-25

## Introduction and how you can help

Thanks for visiting the direction page for Importers in GitLab. This page belongs to the [Import](https://about.gitlab.com/handbook/product/product-categories/#import-group) group of the [Manage](https://about.gitlab.com/direction/manage/) stage and is maintained by [Haris Delalić](https://gitlab.com/hdelalic) who can be contacted directly via [email](mailto:hdelalic@gitlab.com). This vision is a work in progress and everyone can contribute. If you'd like to provide feedback or contribute to this vision, please feel free to comment directly on issues and epics at GitLab.com.

## Mission

The mission of the Importers category is to provide a great experience importing from other applications in our customer's DevOps toolchains, thereby removing friction for migrating to GitLab. Our goal is to build the Importers that our customers find valuable, reliable and easy to use in order to create a more positive first impression when migrating to GitLab. This also includes GitLab-to-GitLab migrations, particularly self-managed GitLab to GitLab.com.  

## Problems to solve

A typical organization looking to adopt GitLab already has many other tools. Artifacts such as code, build pipelines, issues and epics may already exist and are being used daily. Seamless transition of work in progress is critically important and a great experience during this migration creates a positive first impression of GitLab. Solving these transitions, even for the complex cases, is crucial for GitLab’s ability to expand in the market.

At this time, GitLab is targeting the following high-level areas for import: GitLab self-managed to GitLab.com, planning tools, CI/CD tools, and source control tools.

The Manage:Import group that is responsible for this direction page is currently focused on GitLab self-managed to GitLab.com import/export and our source code importers such as GitHub and Bitbucket. At this point in time the [Jira Importer](https://about.gitlab.com/direction/plan/jira_importer/) is owned by the Plan:Project Management group and the [Jenkins Importer](https://about.gitlab.com/direction/verify/jenkins_importer/) is owned by the Verify:Continuous Integration group.

In addition to the migration from other tools, creating easy and reliable GitLab-to-GitLab migrations allows our customers to choose how they access GitLab. While Projects can be migrated using the UI, the ability to migrate GitLab Groups is only available via the API, which requires custom work and manual intervention. With GitHost being deprecated, it is even more important to have a robust migration solution that allows our customers to easily migrate to GitLab.com.

#### Areas of focus:
##### Importer coverage
Offering importers for the main competitors is crucial to enabling sales when competing against an incumbant solution. Our data shows that the most used importers for 3rd party solutions are GitHub, Bitbucket Cloud and Git importers. In addition to the GitLab importer, these importers are our main focus. Additionally, we will continue to evaluate building importers for solutions that we currently don't support, such as Trello and Azure DevOps.

##### Reliability
A large portion of our current importer issues is about the reliability of the solution. Our imports don't always succeed and when they fail, there is little guidance for the user on steps they can take to remedy the failure. We will need to make our Importers more reliable for our customers to have confidence in the migration process.

##### Ease of use
The current importer flow is hard to discover and not always there where the users are looking for it. The user experience is not always user friendly and making improvements in this area will make our importers more lovable.

## Maturity Plan

Importers is a **non-marketable category**, and is therefore not assigned a maturity level. However, we use [GitLab's Maturity framework](https://about.gitlab.com/direction/maturity/) to visualize the current state of the high priority importers and discuss their future roadmap. Generally speaking:

* A Minimal importer is only usable via the API, with only basic importing capabilities and no UI controls.
* A Viable importer has a UI component, but imports an incomplete set of objects. Users are not included.
* A Complete importer is stable, handles large-scale imports, and imports over most objects.
* A Lovable importer has a terrific user experience, recovers gracefully from errors, and imports over nearly 100% of relevant objects.

#### GitLab Importer

The GitLab Importer functionality currently consists of two separate importers, the [Project Importer](https://docs.gitlab.com/ee/user/project/settings/import_export.html) and the [Group Importer](https://docs.gitlab.com/ee/user/group/settings/import_export.html). Together, they allow for Group and Project migrations between two instances of GitLab.

The GitLab Importer is currently a **Viable** feature in GitLab. Both the Group and the Project importers are available through the API and the UI and most of the objects in each are being exported and imported. 

We are currently focused on **Complete** maturity for GitLab Importer, which is being planned in [this epic](https://gitlab.com/groups/gitlab-org/-/epics/2901). This maturity level will include the ability to migrate users, which has been identified as the highest priority gap in the functionality.

#### GitHub Importer

The [GitHub Importer](https://docs.gitlab.com/ee/user/project/import/github.html) is currently a **Viable** feature in GitLab. It is accessible through both the API and the UI and uses a direct link to the GitHub server to import the highest priority objects, such as merge requests, issues, milestones and wiki pages, in addition to the repository. 

We are currently in the [planning stages](https://gitlab.com/groups/gitlab-org/-/epics/2984) to reach the **Complete** maturity level for GitHub Importer. This maturity level will include most of the available objects, such as groups, snippets and members.

#### Bitbucket Cloud Importer

The [Bitbucket Cloud Importer](https://docs.gitlab.com/ee/user/project/import/bitbucket.html) is currently a **Viable** feature in GitLab. It is accessible through the UI and uses a direct link to the Bitbucket Cloud repository to import the highest priority objects, such as merge requests, issues, and milestones, in addition to the repository. 

There is currently no ongoing work to achieve the **Complete** maturity level for Bitbucket Cloud Importer. This maturity level would include most of the available objects and ability to invoke the import via the API.

#### Git Importer

The [Git Importer](https://docs.gitlab.com/ee/user/project/import/repo_by_url.html) is currently a **Complete** feature in GitLab. This is a basic importer that migrates any Git repository into GitLab. Most of our competitors (i.e. GitHub, Microsoft Azure, Atlassian Bitbucket) only offer this type of importer.

There is currently no ongoing work to achieve the **Lovable** maturity level for Git Importer. This maturity level would include an improved user experience.

## What's next & why

To provide a path for our customers moving from GitHost to GitLab.com, the Import group is currently focused on enabling GitLab.com adoption by [starting to iterate](https://gitlab.com/gitlab-org/gitlab/-/issues/224533) toward the [Complete solution](https://gitlab.com/groups/gitlab-org/-/epics/2901) for GitLab-to-GitLab migrations.

Additionally, the Import group continues to enhance the overall user experience by [improving the UI for creating new Projects](https://gitlab.com/groups/gitlab-org/-/epics/3684), which is the starting point for our Importers. These improvements will make our import options more visible and increase the amount of new projects created via an import.

After this, we'll be focused on improving our GitHub to GitLab importer by iterating our way towards a [complete GitHub importer](https://gitlab.com/groups/gitlab-org/-/epics/2984).

## What is not planned right now

Group Import continuously evaluates and updates the Importers' direction and roadmap. As part of that effort, new Importers such as Trello, CircleCI, Subversion and Azure DevOps are being discussed. While these discussions may ultimately lead to the implementation of a new feature or a new Importer, none of them are being planned at this time.

#### Note
* While the Import group’s main focus is Importers, other groups may choose to contribute to individual Importers based on their strategic importance and adoption of their features. This is in keeping with GitLab’s mission that [everyone can contribute](https://about.gitlab.com/handbook/values/#mission). 

In addition to the importers maintained by the Import group, the Professional Services group has developed [Congregate](https://gitlab.com/gitlab-com/customer-success/tools/congregate), a tool that can be used to coordinate and customize large migration projects.

Congregate relies on the Importer code and the API. However, when there is functionality needed that is deemed “core” to the Importers, effort is made to coordinate that change with the Import group and contribute that functionality to the base Importer.

If you'd like to contribute feedback on areas you'd like to see prioritized, please add them as comments in the corresponding [epic](https://gitlab.com/groups/gitlab-org/-/epics/2721) for this category.
